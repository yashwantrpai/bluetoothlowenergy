package com.bignerdranch.android.bluetoothtestbed;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;
    }

    public static Context getContext(){
        return appContext;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}