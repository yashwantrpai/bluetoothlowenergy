package com.bignerdranch.android.bluetoothtestbed.server;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.android.bluetoothtestbed.R;
import com.bignerdranch.android.bluetoothtestbed.server.serverfragment;

import java.util.ArrayList;

import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.getCharacteristicStringId;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCharacteristic extends Fragment {

    ArrayList<String> text = new ArrayList<>();
    RecyclerView Characteristic_recycler_View;
    CharacteristicsAdapter Adapter;
    public View rootView;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public String[] characteristic_list;
    public SelectCharacteristic() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.select_characteristic, container, false);

        pref = getActivity().getSharedPreferences("Preferences", getContext().MODE_PRIVATE);
        editor = pref.edit();
        text.clear();

        Characteristic_recycler_View = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        Adapter = new CharacteristicsAdapter(text);
        Characteristic_recycler_View.setAdapter(Adapter);
        Characteristic_recycler_View.setHasFixedSize(true);
        Characteristic_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));

        characteristic_list = getResources().getStringArray(R.array.characteristic_list);

        try{
            for(String characteristic: characteristic_list){
                setNewLayout(characteristic);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return rootView;
    }

    public void setNewLayout(String txt){
        text.add(txt);

        Adapter.notifyDataSetChanged();
    }

    public class CharacteristicsAdapter extends RecyclerView.Adapter<CharacteristicsAdapter.MyViewHolder> {
        ArrayList<String> Text = new ArrayList<>();
        View layout_view;

        public CharacteristicsAdapter(ArrayList<String> text) {
            this.Text = text;
        }

        @Override
        public CharacteristicsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            layout_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new CharacteristicsAdapter.MyViewHolder(layout_view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            try {
                holder.text.setText(Text.get(position).toString());
                layout_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editor.putString("characteristic", characteristic_list[position]);
                        editor.putString("position", String.valueOf(position));
                        editor.commit();
                        goToServerFragment();
                    }
                });
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return text.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;

            public MyViewHolder(View view) {
                super(view);
                text = (TextView) view.findViewById(R.id.text);
            }
        }
    }

    public void goToServerFragment(){
        Fragment fragment = new serverfragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }
}
