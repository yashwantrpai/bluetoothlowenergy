package com.bignerdranch.android.bluetoothtestbed;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.android.bluetoothtestbed.client.ClientActivity;
import com.bignerdranch.android.bluetoothtestbed.databinding.ActivityMainBinding;
import com.bignerdranch.android.bluetoothtestbed.server.ServerActivity;
import com.bignerdranch.android.bluetoothtestbed.util.Util;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Activity activity;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static AlertDialog ServerDetailsAlert;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;

        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorNotification));
        }
        pref = this.getSharedPreferences("Preferences", MODE_PRIVATE);
        editor = pref.edit();

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.launchServerButton.setOnClickListener(v -> EnterServerDetails(this));
        binding.launchClientButton.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, ClientActivity.class)));
    }
    public  void EnterServerDetails(Activity activity)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.serverdetails, null);


        final EditText blename_view = (EditText) dialogView.findViewById(R.id.blename);
        final EditText vin_view = (EditText) dialogView.findViewById(R.id.vin);
        final EditText imei_view=(EditText)dialogView.findViewById(R.id.imei);
        final Button save = (Button) dialogView.findViewById(R.id.save);
        final Button cancel = (Button) dialogView.findViewById(R.id.cancel);
        final RadioButton none_btn=(RadioButton)dialogView.findViewById(R.id.none);
        final RadioButton prod_btn=(RadioButton)dialogView.findViewById(R.id.production);
        final  RadioButton test_btn=(RadioButton)dialogView.findViewById(R.id.testserver);



        String existing_blename=pref.getString("servername","");
        String existing_vin=pref.getString("vin","");
        String existing_imei=pref.getString("imei","");
        String servertype=pref.getString("server","");
        if(servertype.equalsIgnoreCase("none"))
        {
            none_btn.setChecked(true);
        }else if(servertype.equalsIgnoreCase("prod"))
        {
           prod_btn.setChecked(true);
        }else if(servertype.equalsIgnoreCase("test"))
        {
            test_btn.setChecked(true);
        }

        blename_view.setText(existing_blename);
        vin_view.setText(existing_vin);
        imei_view.setText(existing_imei);

        dialogBuilder.setView(dialogView);
        ServerDetailsAlert = dialogBuilder.create();
        ServerDetailsAlert.setCanceledOnTouchOutside(false);
        ServerDetailsAlert.setCancelable(false);
        ServerDetailsAlert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface)
            {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                boolean valid=false;
                String new_blename=blename_view.getText().toString();
                String new_vin=vin_view.getText().toString();
                new_vin=new_vin.toUpperCase();
                String new_imei=imei_view.getText().toString();
                ArrayList<String>invalidlist=new ArrayList<>();
                try
                {
                    if (Util.vinCheck(new_vin))
                    {
                        valid=true;

                    }
                }catch (Exception ex)
                {
                    valid=false;
                    ex.printStackTrace();
                }
                if(valid&&Util.NotNull(new_imei)&&Util.NotNull(new_blename))
                {
                    valid=true;

                }else
                {
                    valid=false;

                }

                if(valid&&(none_btn.isChecked()||prod_btn.isChecked()||test_btn.isChecked()))
                {
                    String server_type = "";
                    if (none_btn.isChecked()) {
                        server_type = "none";
                    } else if (prod_btn.isChecked()) {
                        server_type = "prod";
                    } else if (test_btn.isChecked()) {
                        server_type = "test";
                    }
                    editor.putString("vin", new_vin);
                    editor.putString("imei", new_imei);
                    editor.putString("servername", new_blename);
                    editor.putString("server", server_type);
                    editor.commit();
                    if(none_btn.isChecked())
                    {
                        Toast.makeText(getApplicationContext(),"Please select a valid server type",Toast.LENGTH_LONG).show();

                    }else {



                        ServerDetailsAlert.dismiss();
                        startActivity(new Intent(MainActivity.this, ServerActivity.class));
                    }

                }else
                {
                    Toast.makeText(getApplicationContext(),"Please enter a valid  Bluetooth name ,VIN ,IMEI and server type",Toast.LENGTH_LONG).show();
                }



            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServerDetailsAlert.dismiss();
            }
        });
        ServerDetailsAlert.show();
    }
}
