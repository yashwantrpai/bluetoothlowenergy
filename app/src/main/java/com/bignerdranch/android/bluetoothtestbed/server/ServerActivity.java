package com.bignerdranch.android.bluetoothtestbed.server;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.content.SharedPreferences;
import android.view.Window;
import android.view.WindowManager;


import com.bignerdranch.android.bluetoothtestbed.R;

public class ServerActivity extends AppCompatActivity {

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    private static final String TAG = "ServerActivity";
    Activity activity;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);
        activity = this;

        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorNotification));
        }

        pref = this.getSharedPreferences("Preferences", MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("characteristic", "");
        editor.putString("position", "");

        String[] characteristic_ids = getResources().getStringArray(R.array.characteristic_ids);
        String[] default_values = getResources().getStringArray(R.array.default_values);
        serverfragment.cellular_data_send=false;

        editor.putString("gps_info", "0");
        editor.putString("speed", "0");
        editor.putString("true_odo", "0");
        editor.putString("derived_odo", "0");
        editor.putString("eng_ign", "0");
        editor.putString("rpm", "0");
        editor.putString("protocol", "1");
        editor.putString("eng_hours", "0");
        editor.putString("moving", "0");
        editor.putString("ecm", "0");
        editor.putString("fuel_level", "0");
        editor.putString("eng_run_time", "0");
        editor.putString("ble_watchdog_interval", "0");
        editor.putString("engine_hours_seconds", "0");
        editor.putString("external_power", "0");
        editor.putString("start_trip_gps_info", "0");
        editor.putString("start_trip_true_odo", "0");
        editor.putString("start_trip_eng_hours_seconds", "0");
        editor.putString("ecu_eng_hours_seconds", "0");
        editor.putString("start_trip_ecu_eng_hours_seconds", "0");
        editor.putString("start_trip_system_unix_time", "0");
        editor.putString("system_unix_time", "0");
        editor.putString("driver_id", "0");
        editor.putString("start_trip_derived_odom", "0");
        editor.commit();

        loadHomeFragment();
    }

    public void loadHomeFragment(){
        Fragment fragment = new serverfragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }
}

