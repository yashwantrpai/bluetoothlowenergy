package com.bignerdranch.android.bluetoothtestbed.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.bignerdranch.android.bluetoothtestbed.server.GPSInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class Util {

    public static long timeDiffInSeconds(Date date1, Date date2){
        boolean errors = false;
        long diffInMill = 0;
        if(date1.after(date2)){
            diffInMill = Math.abs(date1.getTime()- date2.getTime());
        } else {
            diffInMill = Math.abs(date2.getTime()- date1.getTime());
        }
        long seconds = TimeUnit.SECONDS.convert(diffInMill, TimeUnit.MILLISECONDS);

        return seconds;
    }

    public static Queue<GPSInfo> getAllGps(){
        Queue<GPSInfo> queue = new LinkedList<>();
        String gps = "41.302227, -119.807117|40.922658, -119.854643|46.556499, -117.974154|45.745619, -117.988846|45.022126, -118.035659|44.467014, -118.070166|43.856583, -118.068363|43.194009, -118.144906|42.560009, -118.217505|40.625759, -118.857956|37.648018, -121.760095|39.671282, -119.207585|37.147897, -121.755048|36.856243, -121.583584|36.456789, -121.526114|36.075317, -121.225187|35.881802, -121.098248|35.672551, -120.984847|40.321304, -118.374844|34.570991, -120.084224";
        String[] inputs = gps.split("\\|");
        for(String input : inputs){
            if(input != null){
                String[] values = input.split(",");
                if(values != null && values.length == 2){
                    GPSInfo gpsInfo = new GPSInfo();
                    double lat = Double.parseDouble(values[0].trim());
                    double lng = Double.parseDouble(values[1].trim());
                    gpsInfo.setLatitude(decimalToHex(lat));
                    gpsInfo.setLongitude(decimalToHex(lng));
                    queue.add(gpsInfo);
                }
            }
        }
        return queue;
    }

    public static String decimalToHex(double signedDecimal){
        long revDecimal = Long.valueOf(String.valueOf(signedDecimal).replace(".", ""));
        String hex = Long.toHexString(revDecimal);
        hex = padLeft(hex, 8).toUpperCase();
        if(hex.length() > 8){
            hex = hex.substring(hex.length() - 8);
        }
        String[] values = splitToNChar(hex, 2);
        StringBuilder str = new StringBuilder()
                .append(values[3])
                .append(values[2])
                .append(values[1])
                .append(values[0]);
        return str.toString();
    }

    private static String[] splitToNChar(String text, int size)
    {
        List<String> parts = new ArrayList<>();
        int length = text.length();
        for (int i = 0; i < length; i += size)
        {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s).replace(' ', '0');
    }
    public static boolean NotNull(String response) {
        boolean result = false;
        try {
            if (response != null && (response.length() > 1 || !(response.isEmpty()) || !(response.equals("")))) {
                result = true;
            }
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }

    public static boolean vinCheck(String vin) throws Exception{
        Map<Character, Integer> vinMap = new HashMap<>();
        Map<Integer, Integer> intMap = new HashMap<>();

        vin=vin.replaceAll("-","");

        vinMap.put('A',1);
        vinMap.put('B',2);
        vinMap.put('C',3);
        vinMap.put('D',4);
        vinMap.put('E',5);
        vinMap.put('F',6);
        vinMap.put('G',7);
        vinMap.put('H',8);
        vinMap.put('J',1 );
        vinMap.put('K',2 );
        vinMap.put('L',3 );
        vinMap.put('M',4 );
        vinMap.put('N',5 );
        vinMap.put('P',7 );
        vinMap.put('R',9 );
        vinMap.put('S',2 );
        vinMap.put('T',3 );
        vinMap.put('U',4 );
        vinMap.put('V',5 );
        vinMap.put('W',6 );
        vinMap.put('X',7 );
        vinMap.put('Y',8 );
        vinMap.put('Z',9 );
        vinMap.put('0',0 );
        vinMap.put('1',1 );
        vinMap.put('2',2 );
        vinMap.put('3',3 );
        vinMap.put('4',4 );
        vinMap.put('5',5 );
        vinMap.put('6',6 );
        vinMap.put('7',7 );
        vinMap.put('8',8 );
        vinMap.put('9',9 );


        intMap.put(1, 8 );
        intMap.put(2, 7 );
        intMap.put(3, 6 );
        intMap.put(4, 5 );
        intMap.put(5 ,4 );
        intMap.put(6, 3 );
        intMap.put(7, 2 );
        intMap.put(8, 10 );
        intMap.put(9, 0 );
        intMap.put(10, 9);
        intMap.put(11, 8);
        intMap.put(12, 7);
        intMap.put(13, 6);
        intMap.put(14, 5);
        intMap.put(15, 4);
        intMap.put(16, 3);
        intMap.put(17, 2);

        if (vin != null && vin.length() == 17){
            if (vin.contains("I") || vin.contains("O") || vin.contains("Q") ) {
                throw new Exception ("Invalid chars I,O, Q");
            }

        } else {
            throw new Exception ("Length is not 17 chars");
        }
        int totalsum = 0;
        for (int i=0; i< vin.length(); i++)
        {
            int temp = vinMap.get(vin.charAt(i));
            int positionValue = intMap.get(i+1);
            int sum = temp  * positionValue;
            totalsum = totalsum + sum;

            System.out.println(vin.charAt(i) + " : " + temp + " : " + positionValue +  " : "  + sum);
        }
        int modSum = totalsum % 11;

        System.out.println("Total sum : "+totalsum+" Mod values: " + modSum);

        int ninethPositionValue = 0;
        try {
            if (modSum >=0 && modSum <= 9){
                ninethPositionValue = Character.getNumericValue(vin.charAt(8));
                if (modSum == ninethPositionValue) {
                    return true;
                }
            }
            if(modSum == 10){
                if(vin.charAt(8)=='X')
                {
                    return true;
                }

            }


            //System.out.println("9th position value: " + ninethPositionValue);
        } catch(NumberFormatException ex) {
            ex.printStackTrace();
        }



        return false;

    }

}
