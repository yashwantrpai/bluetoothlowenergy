package com.bignerdranch.android.bluetoothtestbed.util;

import android.bluetooth.BluetoothGattCharacteristic;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bignerdranch.android.bluetoothtestbed.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * This class is meant to be a replacement for TextUtils to allow unit testing
 * of files that may want to use common TextUtils methods.
 */
public class StringUtils {

    private static final String TAG = "StringUtils";

    private static String byteToHex(byte b) {
        char char1 = Character.forDigit((b & 0xF0) >> 4, 16);
        char char2 = Character.forDigit((b & 0x0F), 16);

        return String.format("0x%1$s%2$s", char1, char2);
    }

    public static String byteArrayInHexFormat(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{ ");
        for (int i = 0; i < byteArray.length; i++) {
            if (i > 0) {
                stringBuilder.append(", ");
            }
            String hexString = byteToHex(byteArray[i]);
            stringBuilder.append(hexString);
        }
        stringBuilder.append(" }");

        return stringBuilder.toString();
    }

    public static byte[] bytesFromString(String string) {
        byte[] stringBytes = new byte[0];
        try {
            stringBytes = string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to convert message string to byte array");
        }

        return stringBytes;
    }

    public static String valueInHex(String strvalue){
        if(strvalue != null && !strvalue.equalsIgnoreCase("")) {
            return Integer.toHexString(Integer.parseInt(strvalue));
        }
        return "";
    }

    public static String valueInHex(int intvalue){
        return Integer.toHexString(intvalue);
    }

    public static int toASCII(char chr){
        return (int) chr;
    }

    @Nullable
    public static String stringFromBytes(byte[] bytes) {
        String byteString = null;
        try {
            byteString = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Unable to convert message bytes to string");
        }
        return byteString;
    }

    public static int getCharacteristicStringId(String characteristicid){
        int characteristicStringId = R.string.gps_info;
        switch (characteristicid){
            case "gps_info":
                characteristicStringId = R.string.gps_info;
                break;

            case "speed":
                characteristicStringId = R.string.speed;
                break;

            case "true_odo":
                characteristicStringId = R.string.true_odo;
                break;

            case "derived_odo":
                characteristicStringId = R.string.derived_odo;
                break;

            case "eng_ign":
                characteristicStringId = R.string.eng_ign;
                break;

            case "rpm":
                characteristicStringId = R.string.rpm;
                break;

            case "protocol":
                characteristicStringId = R.string.protocol;
                break;

            case "vin":
                characteristicStringId = R.string.vin;
                break;

            case "eng_hours":
                characteristicStringId = R.string.eng_hours;
                break;

            case "moving":
                characteristicStringId = R.string.moving;
                break;

            case "ecm":
                characteristicStringId = R.string.ecm;
                break;

            case "fuel_level":
                characteristicStringId = R.string.fuel_level;
                break;

            case "eng_run_time":
                characteristicStringId = R.string.eng_run_time;
                break;

            case "ble_watchdog_interval":
                characteristicStringId = R.string.ble_watchdog_interval;
                break;

            case "engine_hours_seconds":
                characteristicStringId = R.string.engine_hours_seconds;
                break;

            case "external_power":
                characteristicStringId = R.string.external_power;
                break;

            case "start_trip_gps_info":
                characteristicStringId = R.string.start_trip_gps_info;
                break;

            case "start_trip_true_odo":
                characteristicStringId = R.string.start_trip_true_odo;
                break;

            case "start_trip_eng_hours_seconds":
                characteristicStringId = R.string.start_trip_eng_hours_seconds;
                break;

            case "ecu_eng_hours_seconds":
                characteristicStringId = R.string.ecu_eng_hours_seconds;
                break;

            case "start_trip_ecu_eng_hours_seconds":
                characteristicStringId = R.string.start_trip_ecu_eng_hours_seconds;
                break;

            case "start_trip_system_unix_time":
                characteristicStringId = R.string.start_trip_system_unix_time;
                break;

            case "system_unix_time":
                characteristicStringId = R.string.system_unix_time;
                break;

            case "driver_id":
                characteristicStringId = R.string.driver_id;
                break;

            case "start_trip_derived_odom":
                characteristicStringId = R.string.start_trip_derived_odom;
                break;
            case "imei":
                characteristicStringId=R.string.imei;
                break;

            default:
                characteristicStringId = R.string.gps_info;
        }
        return characteristicStringId;
    }

    public static int getStaticCharacteristicValuesArrayId(String characteristic){
        int staticCharacteristicValuesArrayId = R.array.protocol_values;

        switch (characteristic){
            case "eng_ign":
                staticCharacteristicValuesArrayId = R.array.eng_ign_values;
                break;

            case "protocol":
                staticCharacteristicValuesArrayId = R.array.protocol_values;
                break;

            case "moving":
                staticCharacteristicValuesArrayId = R.array.moving_values;
                break;

            case "ecm":
                staticCharacteristicValuesArrayId = R.array.ecm_values;
                break;

            case "external_power":
                staticCharacteristicValuesArrayId = R.array.external_power_values;
                break;

            default:
                staticCharacteristicValuesArrayId = R.array.protocol_values;
        }
        return staticCharacteristicValuesArrayId;
    }


    public static String getPrefStringId(String uuidStr){
        String prefStr = "gps_info";
        switch (uuidStr){
            case "e7737830-1018-11e6-a148-3e1d05defe78":
                prefStr = "gps_info";
                break;

            case "5eed6ea2-0390-11e5-8418-1697f925ec7b":
                prefStr = "speed";
                break;

            case "5eed6d4e-0390-11e5-8418-1697f925ec7b":
                prefStr = "true_odo";
                break;

            case "5eed6719-0390-11e5-8418-1697f925ec7b":
                prefStr = "derived_odo";
                break;

            case "5eed665c-0390-11e5-8418-1697f925ec7b":
                prefStr = "eng_ign";
                break;

            case "31e6e24b-f2fb-4bb9-a16b-9d17a9c4e4ad":
                prefStr = "rpm";
                break;

            case "5eed6477-0390-11e5-8418-1697f925ec7b":
                prefStr = "protocol";
                break;

            case "a9b9f487-5e60-43d5-a249-4d1d3f317d7e":
                prefStr = "vin";
                break;

            case "95dbace5-fcee-467f-bbe9-fe42e195bb04":
                prefStr ="eng_hours";
                break;

            case "5eed659a-0390-11e5-8418-1697f925ec7b":
                prefStr = "moving";
                break;

            case "5eed70e6-0390-11e5-8418-1697f925ec7b":
                prefStr = "ecm";
                break;

            case "1c3917a6-7d33-4152-9a33-858b6f1fc99b":
                prefStr = "fuel_level";
                break;

            case "b71eb3aa-4ed7-4990-ab22-ced09cf58f34":
                prefStr = "eng_run_time";
                break;

            case "2ac307de-fc9b-4450-be6b-77b890f213d8":
                prefStr = "ble_watchdog_interval";
                break;

            case "87e22dd2-e27b-4094-968a-03e1537e7eb7":
                prefStr = "engine_hours_seconds";
                break;

            case "0dd2af9d-7220-474f-8b34-4a94a6a97498":
                prefStr = "external_power";
                break;

            case "d7b083dd-bf65-44e5-9fa1-959bddf2af0b":
                prefStr = "start_trip_gps_info";
                break;

            case "ba4ac777-52ec-4133-9f18-adad8b98f142":
                prefStr = "start_trip_true_odo";
                break;

            case "40418a44-9b8d-49f8-8ef2-afcea1643786":
                prefStr = "start_trip_eng_hours_seconds";
                break;

            case "0d407e4c-39c9-11e8-b467-0ed5f89f718b":
                prefStr = "ecu_eng_hours_seconds";
                break;

            case "d4fb1556-39d2-11e8-b467-0ed5f89f718b":
                prefStr = "start_trip_ecu_eng_hours_seconds";
                break;

            case "0f834728-39d5-11e8-b467-0ed5f89f718b":
                prefStr = "start_trip_system_unix_time";
                break;

            case "48969a7e-39d5-11e8-b467-0ed5f89f718b":
                prefStr = "system_unix_time";
                break;

            case "66aa658a-0003-4cee-86f0-4c775bbe5a54":
                prefStr = "driver_id";
                break;

            case "5ceb24b2-c273-40b4-b84b-f499e14c3f09":
                prefStr = "start_trip_derived_odom";
                break;

            default:
                prefStr = "gps_info";
        }
        return prefStr;
    }

    public static int getCharacteristicProperties(String characteristicid){
        switch (characteristicid){
            case "gps_info":
                return BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY;

            case "speed":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "true_odo":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "derived_odo":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "eng_ign":
                return BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY;

            case "rpm":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "protocol":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "vin":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "eng_hours":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "moving":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "ecm":
                return BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY;

            case "fuel_level":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "eng_run_time":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "ble_watchdog_interval":
                return BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE;

            case "engine_hours_seconds":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "external_power":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "start_trip_gps_info":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "start_trip_true_odo":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "start_trip_eng_hours_seconds":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "ecu_eng_hours_seconds":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "start_trip_ecu_eng_hours_seconds":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "start_trip_system_unix_time":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "system_unix_time":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            case "driver_id":
                return BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE;

            case "start_trip_derived_odom":
                return BluetoothGattCharacteristic.PROPERTY_READ;

            default:
                return BluetoothGattCharacteristic.PROPERTY_READ;
        }
    }

    public static String makeDoubleDigit(String singledigit){
        int digit = Integer.parseInt(singledigit);
        String doubledigit = "00";
        if(digit < 10){
            doubledigit = "0" + String.valueOf(singledigit);
        }
        return doubledigit;
    }
}
