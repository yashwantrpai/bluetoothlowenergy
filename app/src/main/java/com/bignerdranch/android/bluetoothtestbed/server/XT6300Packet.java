package com.bignerdranch.android.bluetoothtestbed.server;

public class XT6300Packet {
    private String deviceId;
    private String packetId;
    private String reasonCode;
    private String datetimeStamp;
    private String latitude;
    private String longitude;
    private String altitude;
    private String speed;
    private String heading;
    private String rpm;
    private String numstats;
    private String hdop;
    private String derivedOdo;
    private String voltage;
    private String receiverSigStr;
    private String packetSerialNumber;
    private String vin;
    private String systemStates;
    private String odbFuellevelPct;
    private String obdTrueOdometer;
    private String obdTotEngSecs;
    private String obdTotDrivingSec;
    private String startDerivedOdo;
    private String startTrueEngineHours;
    private String startTrueOdo;
    private String obdLifeTimeodom;
    private String obdProtocols;
    private String obdCommsState;
    private String imei;
    private String obdEngineSecs;
    private String obdEngCoolantTemp;
    private String startDerivedEngineHours;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getDatetimeStamp() {
        return datetimeStamp;
    }

    public void setDatetimeStamp(String datetimeStamp) {
        this.datetimeStamp = datetimeStamp;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getRpm() {
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getNumstats() {
        return numstats;
    }

    public void setNumstats(String numstats) {
        this.numstats = numstats;
    }

    public String getHdop() {
        return hdop;
    }

    public void setHdop(String hdop) {
        this.hdop = hdop;
    }

    public String getDerivedOdo() {
        return derivedOdo;
    }

    public void setDerivedOdo(String derivedOdo) {
        this.derivedOdo = derivedOdo;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getReceiverSigStr() {
        return receiverSigStr;
    }

    public void setReceiverSigStr(String receiverSigStr) {
        this.receiverSigStr = receiverSigStr;
    }

    public String getPacketSerialNumber() {
        return packetSerialNumber;
    }

    public void setPacketSerialNumber(String packetSerialNumber) {
        this.packetSerialNumber = packetSerialNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getSystemStates() {
        return systemStates;
    }

    public void setSystemStates(String systemStates) {
        this.systemStates = systemStates;
    }

    public String getOdbFuellevelPct() {
        return odbFuellevelPct;
    }

    public void setOdbFuellevelPct(String odbFuellevelPct) {
        this.odbFuellevelPct = odbFuellevelPct;
    }

    public String getObdTrueOdometer() {
        return obdTrueOdometer;
    }

    public void setObdTrueOdometer(String obdTrueOdometer) {
        this.obdTrueOdometer = obdTrueOdometer;
    }

    public String getObdTotEngSecs() {
        return obdTotEngSecs;
    }

    public void setObdTotEngSecs(String obdTotEngSecs) {
        this.obdTotEngSecs = obdTotEngSecs;
    }

    public String getObdTotDrivingSec() {
        return obdTotDrivingSec;
    }

    public void setObdTotDrivingSec(String obdTotDrivingSec) {
        this.obdTotDrivingSec = obdTotDrivingSec;
    }

    public String getStartDerivedOdo() {
        return startDerivedOdo;
    }

    public void setStartDerivedOdo(String startDerivedOdo) {
        this.startDerivedOdo = startDerivedOdo;
    }

    public String getStartTrueEngineHours() {
        return startTrueEngineHours;
    }

    public void setStartTrueEngineHours(String startTrueEngineHours) {
        this.startTrueEngineHours = startTrueEngineHours;
    }

    public String getStartTrueOdo() {
        return startTrueOdo;
    }

    public void setStartTrueOdo(String startTrueOdo) {
        this.startTrueOdo = startTrueOdo;
    }

    public String getObdLifeTimeodom() {
        return obdLifeTimeodom;
    }

    public void setObdLifeTimeodom(String obdLifeTimeodom) {
        this.obdLifeTimeodom = obdLifeTimeodom;
    }

    public String getObdProtocols() {
        return obdProtocols;
    }

    public void setObdProtocols(String obdProtocols) {
        this.obdProtocols = obdProtocols;
    }

    public String getObdCommsState() {
        return obdCommsState;
    }

    public void setObdCommsState(String obdCommsState) {
        this.obdCommsState = obdCommsState;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getObdEngineSecs() {
        return obdEngineSecs;
    }

    public void setObdEngineSecs(String obdEngineSecs) {
        this.obdEngineSecs = obdEngineSecs;
    }

    public String getObdEngCoolantTemp() {
        return obdEngCoolantTemp;
    }

    public void setObdEngCoolantTemp(String obdEngCoolantTemp) {
        this.obdEngCoolantTemp = obdEngCoolantTemp;
    }


    public String getStartDerivedEngineHours() {
        return startDerivedEngineHours;
    }

    public void setStartDerivedEngineHours(String startDerivedEngineHours) {
        this.startDerivedEngineHours = startDerivedEngineHours;
    }

}
