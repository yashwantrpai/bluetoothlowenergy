package com.bignerdranch.android.bluetoothtestbed.server;

public enum PropertyEnum {

    GPS_Info("gps_info"),
    Speed("speed"),
    True_Odo("true_odo"),
    Derived_Odo("derived_odo"),
    Eng_Ign("eng_ign"),
    RPM("rpm"),
    Protocol("protocol"),
    VIN("vin"),
    Eng_Hours("eng_hours"),
    Moving("moving"),
    ECM("ecm"),
    Fuel_Level("fuel_level"),
    Eng_Run_Time("eng_run_time"),
    Eng_Hours_Seconds("engine_hours_seconds"),
    Start_Trip_GPS_info("start_trip_gps_info"),
    Start_Trip_True_Odo("start_trip_true_odo"),
    Start_Trip_Eng_Hours_Seconds("start_trip_eng_hours_seconds"),
    ECU_Eng_Hours_Seconds("ecu_eng_hours_seconds"),
    Start_Trip_ECU_Eng_Hours_Seconds("start_trip_ecu_eng_hours_seconds"),
    Start_Trip_System_Unix_Time("start_trip_system_unix_time"),
    System_Unix_Time("system_unix_time"),
    Driver_ID("driver_id"),
    Start_Trip_Derived_Odom("start_trip_derived_odom")
    ;

    private final String value;

    private PropertyEnum(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
