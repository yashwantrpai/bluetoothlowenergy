package com.bignerdranch.android.bluetoothtestbed.util;

import java.util.HashMap;

/*public class BluetoothCharacteristicsUtil {

    String[] readWriteCharacteristics = App.appContext.getResources().getStringArray(R.array.read_write_characteristics);
    String[] readNotifyCharacteristics = App.appContext.getResources().getStringArray(R.array.read_notify_characteristics);
    String[] staticCharacteristics = App.appContext.getResources().getStringArray(R.array.static_characteristics);
    String[] characteristicList = App.appContext.getResources().getStringArray(R.array.characteristic_list);
    String[] characteristicUuid = App.appContext.getResources().getStringArray(R.array.characteristic_uuids);
    String[] characteristicCodes = App.appContext.getResources().getStringArray(R.array.characteristic_ids);
    String[] defaultValues = App.appContext.getResources().getStringArray(R.array.default_values);

    public BluetoothCharacteristicsUtil(){

    }

    public HashMap<String, BluetoothCharacteristicPojo> getBluetoothCharacteristics(){
        HashMap<String, BluetoothCharacteristicPojo> bluetoothCharacteristicPojoHashMap = new HashMap<>();

        for(int i = 0; i < characteristicCodes.length; i++){
            BluetoothCharacteristicPojo bluetoothCharacteristicPojo = new BluetoothCharacteristicPojo(characteristicCodes[i],
                    characteristicList[i],
                    getIsStaticCharacteristic(characteristicCodes[i]),
                    getStaticCharacteristicValues(characteristicCodes[i]),
                    getPermissionType(characteristicCodes[i]),
                    characteristicUuid[i],
                    defaultValues[i]);

            bluetoothCharacteristicPojoHashMap.put(characteristicCodes[i], bluetoothCharacteristicPojo);
        }

        return bluetoothCharacteristicPojoHashMap;
    }

    public boolean getIsStaticCharacteristic(String characteristicId){
        boolean isStaticCharacteristic = false;

        for(int i = 0; i < staticCharacteristics.length; i++){
            if(characteristicId.equalsIgnoreCase(staticCharacteristics[i])){
                isStaticCharacteristic = true;
                break;
            }
        }
        return isStaticCharacteristic;
    }

    public String[] getStaticCharacteristicValues(String characteristicId){
        String[] staticCharacteristicValues = null;

        switch(characteristicId){
            case "eng_ign_values":
                staticCharacteristicValues = App.appContext.getResources().getStringArray(R.array.eng_ign_values);
                break;

            case "protocol_values":
                staticCharacteristicValues = App.appContext.getResources().getStringArray(R.array.protocol_values);
                break;

            case "moving_values":
                staticCharacteristicValues = App.appContext.getResources().getStringArray(R.array.moving_values);
                break;

            case "ecm_values":
                staticCharacteristicValues = App.appContext.getResources().getStringArray(R.array.ecm_values);
                break;

            case "external_power_values":
                staticCharacteristicValues = App.appContext.getResources().getStringArray(R.array.external_power_values);
                break;

            default:
                staticCharacteristicValues = null;
        }
        return staticCharacteristicValues;
    }

    public String getPermissionType(String characteristicId){
        String permissionType = "Read";

        if(isReadNotifyCharacteristic(characteristicId)){
            permissionType = "ReadNotify";
        } else if(isReadWriteCharacteristic(characteristicId)){
            permissionType = "ReadWrite";
        }
        return permissionType;
    }

    public boolean isReadNotifyCharacteristic(String characteristicId){
        boolean isReadNotifyCharacteristic = false;
        for(int i = 0; i < readNotifyCharacteristics.length; i++){
            if(characteristicId.equalsIgnoreCase(readNotifyCharacteristics[i])){
                isReadNotifyCharacteristic = true;
                break;
            }
        }
        return isReadNotifyCharacteristic;
    }

    public boolean isReadWriteCharacteristic(String characteristicId){
        boolean isReadWriteCharacteristic = false;
        for(int i = 0; i < readWriteCharacteristics.length; i++){
            if(characteristicId.equalsIgnoreCase(readWriteCharacteristics[i])){
                isReadWriteCharacteristic = true;
                break;
            }
        }
        return isReadWriteCharacteristic;
    }
} */
