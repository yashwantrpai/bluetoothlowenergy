package com.bignerdranch.android.bluetoothtestbed.server;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;

import com.bignerdranch.android.bluetoothtestbed.R;
import com.bignerdranch.android.bluetoothtestbed.server.SelectCharacteristic;
import com.bignerdranch.android.bluetoothtestbed.databinding.ServerfragmentBinding;
import com.bignerdranch.android.bluetoothtestbed.util.BluetoothUtils;
import com.bignerdranch.android.bluetoothtestbed.util.ByteUtils;
import com.bignerdranch.android.bluetoothtestbed.util.StringUtils;
import com.bignerdranch.android.bluetoothtestbed.util.Util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;

import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.getCharacteristicProperties;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.getCharacteristicStringId;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.getPrefStringId;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.getStaticCharacteristicValuesArrayId;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.makeDoubleDigit;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.toASCII;
import static com.bignerdranch.android.bluetoothtestbed.util.StringUtils.valueInHex;

import java.nio.ByteBuffer;
import java.util.Queue;

public class serverfragment extends Fragment {

    public static boolean VIN_EMPTY = false;
    public static boolean PROTOCOL_ZERO_6012 = false;


    View rootView;
    private static final String TAG = "ServerActivity";

    private ServerfragmentBinding mBinding;

    private Handler mHandler;
    private Handler mLogHandler;
    private List<BluetoothDevice> mDevices;
    public SharedPreferences pref;
    private BluetoothGattServer mGattServer;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;
    public boolean isstaticCharacteristic = false;
    public String selectedCharacteristic;
    public SharedPreferences.Editor editor;
    public BluetoothGattService service;
    public static AlertDialog.Builder dialogBuilder;
    public static AlertDialog selectCharacteristicAlert;
    ArrayList<String> text = new ArrayList<>();
    RecyclerView Characteristic_recycler_View;
    public static ConnectivityManager connectivityManager;
    public static NetworkInfo activeNetworkInfo;
    CharacteristicsAdapter Adapter;
    public String[] characteristic_list;
    public static UUID CLIENT_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static long secondsG = 1000;
    public static long odoG = 9999;
    public static long secondsGStart = 1000;
    public static long odoGStart = 9999;

    public static int speed = 0;
    public static boolean blestatus=false;
    public static String vin ="";
    public static String imei="";
    public static int ignition = 0;
    public static long odo=0;
    public static long odoStart=0;
    public static long engineHours=0;
    public static long engineHoursStart=0;
    public static Date odoPrevTimeStamp;
    public static Date engineHoursPrevTimeStamp;
    public static Boolean cellular_data_send=false;

    public static Queue<GPSInfo> gpsQueue = null;

    public serverfragment() {
        // Required empty public constructor
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Adding wake lock
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        dialogBuilder = new AlertDialog.Builder(getContext());
        characteristic_list = getResources().getStringArray(R.array.characteristic_list);



        mHandler = new Handler();
        mLogHandler = new Handler(Looper.getMainLooper());
        mDevices = new ArrayList<>();
        pref = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        editor = pref.edit();
        UUID serviceUUID = java.util.UUID.fromString(getResources().getString(R.string.service_uuid));
        service = new BluetoothGattService(serviceUUID, BluetoothGattService.SERVICE_TYPE_PRIMARY);

        mBluetoothManager = (BluetoothManager) getActivity().getSystemService(getContext().BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        if(gpsQueue == null){
            gpsQueue = Util.getAllGps();
        }

        StringBuilder random = new StringBuilder();
        for(int i=0;i<5;i++){
            random.append(new Random().nextInt(9));
        }

        try{
            odo=Long.parseLong(getValue(PropertyEnum.True_Odo.getValue(), "0"));
            odoStart=Long.parseLong(getValue(PropertyEnum.Start_Trip_True_Odo.getValue(), "0"));
            engineHours=Long.parseLong(getValue(PropertyEnum.ECU_Eng_Hours_Seconds.getValue(), "0"));
            engineHoursStart=Long.parseLong(getValue(PropertyEnum.Start_Trip_ECU_Eng_Hours_Seconds.getValue(), "0"));

        }catch(Exception ex){
            Log.e("error", "error", ex);
        }
        String servername=pref.getString("servername","");
        mBluetoothAdapter.setName(servername);
        //HarpBT183206219
        //mBluetoothAdapter.setName("HarpBT18");

        mBinding = DataBindingUtil.inflate(inflater, R.layout.serverfragment, container, false);
        rootView = mBinding.getRoot();
        mBinding.servername.setText(servername);
        mBinding.restartServerButton.setOnClickListener(v -> restartServer());
        mBinding.viewServerLog.clearLogButton.setOnClickListener(v -> clearLogs());
        mBinding.selectCharacteristicLayout.setOnClickListener(v -> selectCharacteristicDialog());
        mBinding.servernameButton.setOnClickListener(v -> setServerName());
        mBinding.dynamicValue.setInputType(InputType.TYPE_CLASS_TEXT);


        //mBluetoothAdapter.setName(mBinding.servername.getText().toString());


        if(pref.getString("eng_ign", "0").equalsIgnoreCase("0")){
            mBinding.ignitionSwitch.setChecked(false);
        }
        mBinding.cellulardata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isPressed()) {
                    cellular_data_send=isChecked;

                }

            }
        });
        mBinding.vinempty.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(compoundButton.isPressed()) {
                    if (isChecked) {
                        VIN_EMPTY = true;
                    } else {
                        VIN_EMPTY = false;
                    }
                }
            }
        });
        mBinding.protocol.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(compoundButton.isPressed()) {
                    if (isChecked) {
                        PROTOCOL_ZERO_6012 = true;
                    } else {
                        PROTOCOL_ZERO_6012 = false;
                    }
                }
            }
        });
        mBinding.ignitionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {

                activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {
                    Toast.makeText(getContext(),"Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
                }


                if(isChecked){
                    ignition = 1;
                    speed=0;
                    editor.putString("eng_ign", "1");
                    editor.putString("ecm", "3");
                    editor.putString("gps_info", "4");
                    editor.commit();
                    String vin = pref.getString("vin", "");
                    imei=pref.getString("imei","");
                    //secondsG = secondsG + 100;
                    //odoG = odoG + 200;
                    /*secondsGStart = secondsG;
                    odoGStart = odoG;
                    long nowLong =  secondsG;
                    long odo = odoG;*/
                    long time = getSystemTimeInSeconds();
                    odoStart = odo;

                    engineHoursStart = engineHours;
                    updateValue(PropertyEnum.Start_Trip_ECU_Eng_Hours_Seconds.getValue(), String.valueOf(engineHoursStart));
                    //updateValue(PropertyEnum.Start_Trip_Eng_Hours_Seconds.getValue(), String.valueOf(engineHoursStart));
                    updateValue(PropertyEnum.Start_Trip_True_Odo.getValue(), String.valueOf(odoStart));
                    updateValue(PropertyEnum.Start_Trip_Derived_Odom.getValue(), String.valueOf(odoStart));
                    updateValue(PropertyEnum.Start_Trip_System_Unix_Time.getValue(), String.valueOf(time));

                    InetAddress addr = null;
                    DatagramSocket socket = null;
                    int PORT = 4446;
                    byte[] buf;
                    try{
                        addr = InetAddress.getByName(getServerAddress());
                        socket = new DatagramSocket();
                    }catch(Exception ex){
                        Log.e("error", "error", ex);
                    }


                    try {


                        //XT6300Cellular obj = new XT6300Cellular();


                        XT6300Packet packet = new XT6300Packet();
                        packet.setDeviceId(  XT6300Cellular.getDeviceId());
                        packet.setPacketId(XT6300Cellular.getPacketId());


                        packet.setReasonCode(XT6300Cellular.getReasonCode(6011));
                        packet.setDatetimeStamp(XT6300Cellular.getDateTimeStamp());
                        GPSInfo gpsInfo = gpsQueue.poll();
                        packet.setLatitude(XT6300Cellular.getLatitude(37.734430));
                        packet.setLongitude(XT6300Cellular.getLongitude(-121.941373));
                        gpsQueue.add(gpsInfo);
                        packet.setAltitude(XT6300Cellular.getAltitude());
                        packet.setSpeed(XT6300Cellular.getSpeed(speed));
                        packet.setHeading(XT6300Cellular.getHeading());
                        packet.setRpm(XT6300Cellular.getRPM());
                        packet.setNumstats(XT6300Cellular.getNumStats());
                        packet.setHdop(XT6300Cellular.getHdop());
                        packet.setDerivedOdo(XT6300Cellular.getOdoDerived(6011));
                        packet.setVoltage(XT6300Cellular.getVoltage());
                        packet.setReceiverSigStr(XT6300Cellular.getReceiverSigStr());
                        packet.setPacketSerialNumber(XT6300Cellular.getPacketSerialNumber());
                        packet.setVin(XT6300Cellular.getVin(vin));
                        packet.setSystemStates(XT6300Cellular.getSystemStates(blestatus));
                        packet.setOdbFuellevelPct(XT6300Cellular.getObdFuelLevelPct(362));
                        packet.setObdTrueOdometer(XT6300Cellular.getOdoTrue(6011));
                        packet.setObdTotEngSecs(XT6300Cellular.getEngineHoursSecondsTrue(6011));
                        packet.setObdTotDrivingSec(XT6300Cellular.getEngineHoursSecondsTrue(6011));
                        packet.setStartDerivedOdo(XT6300Cellular.getOdoDerivedStart(odo,6011));
                        packet.setStartTrueEngineHours(XT6300Cellular.getEngineHoursSecondsTrueStart(engineHoursStart,6011));
                        packet.setStartTrueOdo(XT6300Cellular.getOdoTrueStart(odoStart,6011));
                        packet.setObdLifeTimeodom(XT6300Cellular.getOdoDerived(6011));
                        packet.setObdProtocols(XT6300Cellular.getProtocols("jbus"));
                        packet.setObdCommsState(XT6300Cellular.getECM(3));
                        packet.setImei(XT6300Cellular.getImei(imei));
                        packet.setObdEngineSecs(XT6300Cellular.getEngineHoursSecondsDerived(6011));
                        packet.setObdEngCoolantTemp(XT6300Cellular.getCoolantTemp());
                        packet.setStartDerivedEngineHours(XT6300Cellular.getEngineHoursSecondsDerivedStart(engineHoursStart,6011));

                        StringBuilder s = new StringBuilder();
                        s.append("DeviceId: ").append(packet.getDeviceId()).append(", ");
                        s.append("PacketId: ").append(packet.getPacketId()).append(", ");
                        s.append("ReasonCode: ").append(packet.getReasonCode()).append(", ");
                        s.append("Datetime: ").append(packet.getDatetimeStamp()).append(", ");
                        s.append("Latitude: ").append(packet.getLatitude()).append(", ");
                        s.append("Longitude: ").append(packet.getLongitude()).append(", ");
                        s.append("Altitude: ").append(packet.getAltitude()).append(", ");
                        s.append("Speed: ").append(packet.getSpeed()).append(", ");
                        s.append("Heading: ").append(packet.getHeading()).append(", ");
                        s.append("Rpm: ").append(packet.getRpm()).append(", ");
                        s.append("Numstats: ").append(packet.getNumstats()).append(", ");
                        s.append("Hdop: ").append(packet.getHdop()).append(", ");
                        s.append("Voltage: ").append(packet.getVoltage()).append(", ");
                        s.append("RecieverSigStr: ").append(packet.getReceiverSigStr()).append(", ");
                        s.append("PacketSerialNumber:").append(packet.getPacketSerialNumber()).append(", ");
                        s.append("Vin: ").append(packet.getVin()).append(", ");
                        s.append("Systemstats: ").append(packet.getSystemStates()).append(", ");
                        s.append("Fuellevel: ").append(packet.getOdbFuellevelPct()).append(", ");
                        s.append("Trueodo: ").append(packet.getObdTrueOdometer()).append(", ");
                        s.append("StartTrueOdo: ").append(packet.getStartTrueOdo()).append(", ");
                        s.append("DerivedOdo: ").append(packet.getDerivedOdo()).append(", ");
                        s.append("Ododrivedstart:").append(packet.getStartDerivedOdo()).append(", ");
                        s.append("TrueEngineHours: ").append(packet.getObdEngineSecs()).append(", ");
                        s.append("TrueEngHoursStart: ").append(packet.getStartTrueEngineHours()).append(", ");
                        s.append("DerivedEngineHours: ").append(packet.getObdEngineSecs()).append(" ");
                        s.append("DerivedEngineHoursStart: ").append(packet.getStartDerivedEngineHours()).append(" ");
                        s.append("Imei: ").append(packet.getImei()).append(", ");
                        Log.i("eventDetails",getServerAddress()+" "+s.toString());



                        String data =  XT6300Cellular.constructPacket(packet);

                        Log.i("iii", "test123:" + data);

                        buf = hexStringToByteArray(data);
                        DatagramPacket udpPacket = new DatagramPacket(buf, buf.length, addr, PORT);



                        //packet.setAddress(addr);
                        //packet.setPort(XT6300_PORT);
                        DatagramSocket finalSocket = socket;
                        new Thread(new Runnable(){
                            @Override
                            public void run() {
                                try {
                                    //packet.setAddress(addr);
                                    //packet.setPort(XT6300_PORT);
                                    finalSocket.send(udpPacket);
                                    // Your implementation goes here
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();



                    } catch (Exception ex) {
                        Log.e("error", "error", ex);
                    }



                } else {
                    ignition = 0;
                    vin = pref.getString("vin", "");
                    imei=pref.getString("imei","");
                    editor.putString("eng_ign", "0");
                    editor.putString("ecm", "0");
                    editor.putString("gps_info", "4");

                    editor.commit();

                    //updateValue(PropertyEnum.Start_Trip_ECU_Eng_Hours_Seconds.getValue(), String.valueOf(0));
                    //updateValue(PropertyEnum.Start_Trip_Eng_Hours_Seconds.getValue(), String.valueOf(0));
                    //updateValue(PropertyEnum.Start_Trip_True_Odo.getValue(), String.valueOf(0));
                    //updateValue(PropertyEnum.Start_Trip_Derived_Odom.getValue(), String.valueOf(0));
                    //updateValue(PropertyEnum.Start_Trip_System_Unix_Time.getValue(), String.valueOf(0));
                    updateValue(PropertyEnum.Speed.getValue(), String.valueOf(0));


                    InetAddress addr = null;
                    DatagramSocket socket = null;
                    int PORT = 4446;
                    byte[] buf;
                    try{
                        addr = InetAddress.getByName(getServerAddress());
                        socket = new DatagramSocket();
                    }catch(Exception ex){
                        Log.e("error", "error", ex);
                    }



                    try {


                        XT6300Cellular obj = new XT6300Cellular();
                        XT6300Packet packet = new XT6300Packet();
                        packet.setDeviceId(obj.getDeviceId());
                        packet.setPacketId(obj.getPacketId());


                        packet.setReasonCode(obj.getReasonCode(6012));

                        packet.setDatetimeStamp(obj.getDateTimeStamp());
                        GPSInfo gpsInfo = gpsQueue.poll();
                        packet.setLatitude(obj.getLatitude(37.734430));
                        packet.setLongitude(obj.getLongitude(-121.941373));
                        gpsQueue.add(gpsInfo);
                        packet.setAltitude(obj.getAltitude());
                        packet.setSpeed(obj.getSpeed(speed));
                        packet.setHeading(obj.getHeading());
                        packet.setRpm(obj.getRPM());
                        packet.setNumstats(obj.getNumStats());
                        packet.setHdop(obj.getHdop());
                        packet.setDerivedOdo(obj.getOdoDerived(6012));
                        packet.setVoltage(obj.getVoltage());
                        packet.setReceiverSigStr(obj.getReceiverSigStr());
                        packet.setPacketSerialNumber(obj.getPacketSerialNumber());
                        packet.setVin(obj.getVin(vin));
                        packet.setSystemStates(obj.getSystemStates(blestatus));
                        packet.setOdbFuellevelPct(obj.getObdFuelLevelPct(362));
                        packet.setObdTrueOdometer(obj.getOdoTrue(6012));
                        packet.setObdTotEngSecs(obj.getEngineHoursSecondsTrue(6012));
                        packet.setObdTotDrivingSec(obj.getEngineHoursSecondsTrue(6012));
                        packet.setStartDerivedOdo(obj.getOdoDerivedStart(odo,6012));
                        packet.setStartTrueEngineHours(obj.getEngineHoursSecondsTrueStart(engineHoursStart,6012));
                        packet.setStartTrueOdo(obj.getOdoTrueStart(odoStart,6012));
                        packet.setObdLifeTimeodom(obj.getOdoDerived(6012));
                        packet.setObdProtocols(obj.getProtocols("jbus"));
                        packet.setObdCommsState(obj.getECM(3));
                        packet.setImei(obj.getImei(imei));
                        packet.setObdEngineSecs(obj.getEngineHoursSecondsDerived(6012));
                        packet.setObdEngCoolantTemp(obj.getCoolantTemp());
                        packet.setStartDerivedEngineHours(obj.getEngineHoursSecondsDerivedStart(engineHoursStart,6012));

                        if(PROTOCOL_ZERO_6012){
                            packet.setObdTrueOdometer(obj.getOdoTrue(6012));
                            packet.setStartTrueOdo(obj.getOdoTrueStart(odoStart,6012));
                            packet.setDerivedOdo(obj.getOdoDerived(6012));
                            packet.setStartDerivedOdo(obj.getOdoDerivedStart(odo,6012));
                            packet.setObdLifeTimeodom(obj.getOdoDerived(6012));


                            packet.setObdTotEngSecs(obj.getEngineHoursSecondsTrue(6012));
                            packet.setObdTotDrivingSec(obj.getEngineHoursSecondsTrue(6012));
                            packet.setStartTrueEngineHours(obj.getEngineHoursSecondsTrueStart(engineHoursStart,6012));
                            packet.setObdEngineSecs(obj.getEngineHoursSecondsDerived(6012));
                            packet.setStartDerivedEngineHours(obj.getEngineHoursSecondsDerivedStart(engineHoursStart,6012));
                        }

                        StringBuilder s = new StringBuilder();
                        s.append("DeviceId: ").append(packet.getDeviceId()).append(", ");
                        s.append("PacketId: ").append(packet.getPacketId()).append(", ");
                        s.append("ReasonCode: ").append(packet.getReasonCode()).append(", ");
                        s.append("Datetime: ").append(packet.getDatetimeStamp()).append(", ");
                        s.append("Latitude: ").append(packet.getLatitude()).append(", ");
                        s.append("Longitude: ").append(packet.getLongitude()).append(", ");
                        s.append("Altitude: ").append(packet.getAltitude()).append(", ");
                        s.append("Speed: ").append(packet.getSpeed()).append(", ");
                        s.append("Heading: ").append(packet.getHeading()).append(", ");
                        s.append("Rpm: ").append(packet.getRpm()).append(", ");
                        s.append("Numstats: ").append(packet.getNumstats()).append(", ");
                        s.append("Hdop: ").append(packet.getHdop()).append(", ");
                        s.append("Voltage: ").append(packet.getVoltage()).append(", ");
                        s.append("RecieverSigStr: ").append(packet.getReceiverSigStr()).append(", ");
                        s.append("PacketSerialNumber:").append(packet.getPacketSerialNumber()).append(", ");
                        s.append("Vin: ").append(packet.getVin()).append(", ");
                        s.append("Systemstats: ").append(packet.getSystemStates()).append(", ");
                        s.append("Fuellevel: ").append(packet.getOdbFuellevelPct()).append(", ");
                        s.append("Trueodo: ").append(packet.getObdTrueOdometer()).append(", ");
                        s.append("StartTrueOdo: ").append(packet.getStartTrueOdo()).append(", ");
                        s.append("DerivedOdo: ").append(packet.getDerivedOdo()).append(", ");
                        s.append("Ododrivedstart:").append(packet.getStartDerivedOdo()).append(", ");
                        s.append("TrueEngineHours: ").append(packet.getObdEngineSecs()).append(", ");
                        s.append("TrueEngHoursStart: ").append(packet.getStartTrueEngineHours()).append(", ");
                        s.append("DerivedEngineHours: ").append(packet.getObdEngineSecs()).append(" ");
                        s.append("DerivedEngineHoursStart: ").append(packet.getStartDerivedEngineHours()).append(" ");
                        s.append("Imei: ").append(packet.getImei()).append(", ");
                        Log.i("eventDetails",getServerAddress()+" "+s.toString());

                        String data = obj.constructPacket(packet);

                        Log.i("iii", "test123:" + data);

                        buf = hexStringToByteArray(data);
                        DatagramPacket udpPacket = new DatagramPacket(buf, buf.length, addr, PORT);



                        //packet.setAddress(addr);
                        //packet.setPort(XT6300_PORT);
                        DatagramSocket finalSocket = socket;
                        new Thread(new Runnable(){
                            @Override
                            public void run() {
                                try {
                                    //packet.setAddress(addr);
                                    //packet.setPort(XT6300_PORT);
                                    finalSocket.send(udpPacket);
                                    // Your implementation goes here
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();



                    } catch (Exception ex) {
                        Log.e("error", "error", ex);
                    }



                }




                String selectedCharacteristic = mBinding.selectCharacteristicText.getText().toString();
                if(selectedCharacteristic.equalsIgnoreCase("Eng/Ign")){
                    if(isChecked){
                        mBinding.staticValue.setSelection(1);
                    } else {
                        mBinding.staticValue.setSelection(0);
                    }
                }
                if(selectedCharacteristic.equalsIgnoreCase("ecm")){
                    int ecmTemp = 0;
                    try{
                        ecmTemp = Integer.parseInt(pref.getString("ecm", "0"));
                    }catch (Exception ex) {
                        Log.e("error", "error", ex);
                    }
                    mBinding.staticValue.setSelection(ecmTemp);
                }
                if(selectedCharacteristic.equalsIgnoreCase("gps info")){
                    String gpsinfoTemp = pref.getString("gps_info", "0");
                    mBinding.dynamicValue.setText(gpsinfoTemp);
                }

                Toast.makeText(getContext(), "Ignition updated."  ,Toast.LENGTH_SHORT).show();
            }
        });

        if(pref.getString("moving", "0").equalsIgnoreCase("0")){
            mBinding.movingSwitch.setChecked(false);
        }

        mBinding.movingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String value = "0";
                int nowSpeed = 0;
                if(isChecked){
                    editor.putString("moving", "1");
                    editor.commit();
                } else {
                    editor.putString("moving", "0");
                    editor.commit();
                }
                /*if(isChecked){
                    value = "1";
                    int speedmiles = Integer.parseInt(pref.getString( "speed", "50"));
                    nowSpeed = Math.round( speedmiles * 16);
                }else{
                    int speedmiles = Integer.parseInt(pref.getString( "speed", "2"));
                    nowSpeed = Math.round( speedmiles * 16);
                }
                editor.putString("moving", value);
                editor.putString(PropertyEnum.Speed.getValue(), String.valueOf(nowSpeed));
                editor.commit();*/

                String selectedCharacteristic = mBinding.selectCharacteristicText.getText().toString();
                if(selectedCharacteristic.equalsIgnoreCase("Moving")){
                    if(isChecked){
                        mBinding.staticValue.setSelection(1);
                    } else {
                        mBinding.staticValue.setSelection(0);
                    }
                }

                Toast.makeText(getContext(), "Moving updated to " + value +". Please restart server for changes to take effect.",Toast.LENGTH_SHORT).show();
            }
        });

        setSelectedCharacteristicAndValues();

        // Check if bluetooth is enabled
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            // Request user to enable it
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBtIntent);
            getActivity().finish();
            //return;
        }

        // Check low energy support
        if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            // Get a newer device
            log("No LE Support.");
            getActivity().finish();
            //return;
        }

        // Check advertising
        //https://stackoverflow.com/questions/32092902/why-ismultipleadvertisementsupported-returns-false-when-getbluetoothleadverti
        if (!mBluetoothAdapter.isMultipleAdvertisementSupported()) {
            // Unable to run the server on this device, get a better device
            log("No Advertising Support.");
            getActivity().finish();
            //return;
        }

        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();

        @SuppressLint("HardwareIds")
        String deviceInfo = "Device Info" + "\nName: " + mBluetoothAdapter.getName() + "\nAddress: " + mBluetoothAdapter.getAddress();
        // mBinding.serverDeviceInfoTextView.setText(deviceInfo);

        int state = mBluetoothAdapter.getState();

        if (mBluetoothAdapter.isEnabled() ){
            setupServer();
            startAdvertising();
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    InetAddress addr = null;
                    DatagramSocket socket = null;
                    int PORT = 4446;
                    byte[] buf;
                    try{
                        addr = InetAddress.getByName(getServerAddress());
                        socket = new DatagramSocket();
                    }catch(Exception ex){
                        Log.e("error", "error", ex);
                    }

                    while(true) {
                        try {
                            if(cellular_data_send)
                            {
                                vin = pref.getString("vin", "");
                                imei = pref.getString("imei", "");
                                XT6300Cellular obj = new XT6300Cellular();
                                XT6300Packet packet = new XT6300Packet();
                                packet.setDeviceId(obj.getDeviceId());
                                packet.setPacketId(obj.getPacketId());
                                int eventcode=0;
                                if (ignition == 1) {
                                    eventcode = 4001;
                                    packet.setReasonCode(obj.getReasonCode(eventcode));
                                }
                                if (ignition == 0) {
                                    eventcode = 4002;
                                    packet.setReasonCode(obj.getReasonCode(eventcode));
                                }

                                packet.setDatetimeStamp(obj.getDateTimeStamp());
                                GPSInfo gpsInfo = gpsQueue.poll();
                                packet.setLatitude(obj.getLatitude(37.734430));
                                packet.setLongitude(obj.getLongitude(-121.941373));
                                gpsQueue.add(gpsInfo);
                                packet.setAltitude(obj.getAltitude());
                                packet.setSpeed(obj.getSpeed(speed));
                                packet.setHeading(obj.getHeading());
                                packet.setRpm(obj.getRPM());
                                packet.setNumstats(obj.getNumStats());
                                packet.setHdop(obj.getHdop());
                                packet.setDerivedOdo(obj.getOdoDerived(eventcode));
                                packet.setVoltage(obj.getVoltage());
                                packet.setReceiverSigStr(obj.getReceiverSigStr());
                                packet.setPacketSerialNumber(obj.getPacketSerialNumber());
                                packet.setVin(obj.getVin(vin));
                                packet.setSystemStates(obj.getSystemStates(blestatus));
                                packet.setOdbFuellevelPct(obj.getObdFuelLevelPct(362));
                                packet.setObdTrueOdometer(obj.getOdoTrue(eventcode));
                                packet.setObdTotEngSecs(obj.getEngineHoursSecondsTrue(eventcode));
                                packet.setObdTotDrivingSec(obj.getEngineHoursSecondsTrue(eventcode));
                                packet.setStartDerivedOdo(obj.getOdoDerivedStart(odoStart,eventcode));
                                packet.setStartTrueEngineHours(obj.getEngineHoursSecondsTrueStart(engineHoursStart,eventcode));
                                packet.setStartTrueOdo(obj.getOdoTrueStart(odoStart,eventcode));
                                packet.setObdLifeTimeodom(obj.getOdoDerived(eventcode));
                                packet.setObdProtocols(obj.getProtocols("jbus"));
                                packet.setObdCommsState(obj.getECM(3));
                                packet.setImei(obj.getImei(imei));
                                packet.setObdEngineSecs(obj.getEngineHoursSecondsDerived(eventcode));
                                packet.setObdEngCoolantTemp(obj.getCoolantTemp());
                                packet.setStartDerivedEngineHours(obj.getEngineHoursSecondsDerivedStart(engineHoursStart,eventcode));

                                String data = obj.constructPacket(packet);

                                StringBuilder s = new StringBuilder();
                                s.append("DeviceId: ").append(packet.getDeviceId()).append(", ");
                                s.append("PacketId: ").append(packet.getPacketId()).append(", ");
                                s.append("ReasonCode: ").append(packet.getReasonCode()).append(", ");
                                s.append("Datetime: ").append(packet.getDatetimeStamp()).append(", ");
                                s.append("Latitude: ").append(packet.getLatitude()).append(", ");
                                s.append("Longitude: ").append(packet.getLongitude()).append(", ");
                                s.append("Altitude: ").append(packet.getAltitude()).append(", ");
                                s.append("Speed: ").append(packet.getSpeed()).append(", ");
                                s.append("Heading: ").append(packet.getHeading()).append(", ");
                                s.append("Rpm: ").append(packet.getRpm()).append(", ");
                                s.append("Numstats: ").append(packet.getNumstats()).append(", ");
                                s.append("Hdop: ").append(packet.getHdop()).append(", ");
                                s.append("Voltage: ").append(packet.getVoltage()).append(", ");
                                s.append("RecieverSigStr: ").append(packet.getReceiverSigStr()).append(", ");
                                s.append("PacketSerialNumber:").append(packet.getPacketSerialNumber()).append(", ");
                                s.append("Vin: ").append(packet.getVin()).append(", ");
                                s.append("Systemstats: ").append(packet.getSystemStates()).append(", ");
                                s.append("Fuellevel: ").append(packet.getOdbFuellevelPct()).append(", ");
                                s.append("Trueodo: ").append(packet.getObdTrueOdometer()).append(", ");
                                s.append("StartTrueOdo: ").append(packet.getStartTrueOdo()).append(", ");
                                s.append("DerivedOdo: ").append(packet.getDerivedOdo()).append(", ");
                                s.append("Ododrivedstart:").append(packet.getStartDerivedOdo()).append(", ");
                                s.append("TrueEngineHours: ").append(packet.getObdEngineSecs()).append(", ");
                                s.append("TrueEngHoursStart: ").append(packet.getStartTrueEngineHours()).append(", ");
                                s.append("DerivedEngineHours: ").append(packet.getObdEngineSecs()).append(" ");
                                s.append("DerivedEngineHoursStart: ").append(packet.getStartDerivedEngineHours()).append(" ");
                                s.append("Imei: ").append(packet.getImei()).append(", ");
                                Log.i("eventDetails",getServerAddress()+" "+s.toString());
                                Log.i("iii", "test123:" + data);

                                buf = hexStringToByteArray(data);
                                DatagramPacket udpPacket = new DatagramPacket(buf, buf.length, addr, PORT);

                                //packet.setAddress(addr);
                                //packet.setPort(XT6300_PORT);
                                socket.send(udpPacket);



                            }


                        } catch (Exception ex) {
                            Log.e("error", "error", ex);
                        } finally {
                            try{
                                Thread.sleep(60000);
                            }catch (Exception ex) {
                                Log.e("error", "error", ex);
                            }

                        }
                    }

                }
            });
            //notifyCharacteristic();
        }

        return rootView;
    }

    public void setServerName(){
        String servername = mBinding.servername.getText().toString();
        mBluetoothAdapter.setName(servername);
        editor.putString("servername",servername);
        editor.commit();
        Toast.makeText(getContext(), "BLE server name updated to " + servername, Toast.LENGTH_SHORT).show();
        // restartServer();
    }



    public void setSelectedCharacteristicAndValues(){
        ArrayList<String> staticCharacteristicList = new ArrayList<>();
        if(!pref.getString("characteristic", "").equalsIgnoreCase("")) {
            selectedCharacteristic = pref.getString("characteristic", "");
            mBinding.selectCharacteristicText.setText(selectedCharacteristic);
            isstaticCharacteristic = false;
            String[] static_ids = getResources().getStringArray(R.array.static_characteristics);
            for (String static_id : static_ids) {
                staticCharacteristicList.add(getResources().getString(getCharacteristicStringId(static_id)));
            }

            int position = Integer.parseInt(pref.getString("position", ""));
            String[] characteristicids = getResources().getStringArray(R.array.characteristic_ids);
            for (String staticCharacteristic : staticCharacteristicList) {
                if (selectedCharacteristic.equalsIgnoreCase(staticCharacteristic)) {
                    String[] characteristicValuesArray = getResources().getStringArray(getStaticCharacteristicValuesArrayId(characteristicids[position]));
                    ArrayAdapter<CharSequence> char_adapter = new ArrayAdapter<CharSequence>(getContext(), android.R.layout.simple_spinner_item, characteristicValuesArray);
                    char_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mBinding.staticValue.setAdapter(char_adapter);
                    mBinding.staticValue.setVisibility(View.VISIBLE);
                    if(selectedCharacteristic.equalsIgnoreCase("Moving")){
                        if(pref.getString("moving", "0").equalsIgnoreCase("1")){
                            mBinding.staticValue.setSelection(1);
                        } else {
                            mBinding.staticValue.setSelection(0);
                        }
                    } else if(selectedCharacteristic.equalsIgnoreCase("Eng/Ign")){
                        if(pref.getString("eng_ign", "0").equalsIgnoreCase("1")){
                            mBinding.staticValue.setSelection(1);
                        } else {
                            mBinding.staticValue.setSelection(0);
                        }
                    } else if(selectedCharacteristic.equalsIgnoreCase("ecm")){
                        int ecmselected = 0;
                        try{
                            ecmselected = Integer.parseInt(pref.getString("ecm", "0"));
                        }catch(Exception ex){
                            Log.e("error", "error", ex);
                        }
                        mBinding.staticValue.setSelection(ecmselected);

                    } else if(selectedCharacteristic.equalsIgnoreCase("protocol")){
                        int protocolselected = 0;
                        try{
                            protocolselected = Integer.parseInt(pref.getString("protocol", "0"));
                        }catch(Exception ex){
                            Log.e("error", "error", ex);
                        }
                        mBinding.staticValue.setSelection(protocolselected);

                    } else if(selectedCharacteristic.equalsIgnoreCase("gps info")){
                        /*int gpsinfoselected = 0;
                        try{
                            gpsinfoselected = Integer.parseInt(pref.getString("gps_info", "0"));
                        }catch(Exception ex){
                            Log.e("error", "error", ex);
                        }
                        mBinding.staticValue.setSelection(gpsinfoselected);
                        */
                        mBinding.dynamicValue.setText(pref.getString("gps_info", "0"));
                    }else if(selectedCharacteristic.equalsIgnoreCase("VIN"))
                    {

                        mBinding.dynamicValue.setText(pref.getString("vin",""));
                    }else if(selectedCharacteristic.equalsIgnoreCase("IMEI"))
                    {
                        mBinding.dynamicValue.setText(pref.getString("imei",""));
                    }
                    mBinding.dynamicValue.setVisibility(View.GONE);
                    isstaticCharacteristic = true;
                    break;
                }
                if (selectedCharacteristic.equalsIgnoreCase("gps info")) {
                    String s = pref.getString("gps_info", "4");
                    mBinding.dynamicValue.setText(pref.getString("gps_info", "0"));
                }

            }

            String temp = characteristicids[position];

            mBinding.dynamicValue.setText(pref.getString(characteristicids[position], "0"));

            if(!isstaticCharacteristic){
                mBinding.dynamicValue.setVisibility(View.VISIBLE);
                mBinding.staticValue.setVisibility(View.GONE);
            }

            mBinding.update.setVisibility(View.VISIBLE);
            mBinding.update.setOnClickListener( v -> updateCharacteristics(characteristicids[position]));
        }
    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        stopAdvertising();
        stopServer();
    }

    // GattServer
    private void setupServer() {
        GattServerCallback gattServerCallback = new GattServerCallback();
        mGattServer = mBluetoothManager.openGattServer(getContext(), gattServerCallback);

        String[] characteristic_uuids = getResources().getStringArray(R.array.characteristic_uuids);
        // Read characteristic
        for(String characteristic_uuid: characteristic_uuids){
            BluetoothGattCharacteristic readCharacteristic = createReadCharacteristic(characteristic_uuid);

            service.addCharacteristic(readCharacteristic);
        }

        //mGattServer.clearServices();
        mGattServer.addService(service);
    }

    private void stopServer() {
        if (mGattServer != null) {
            mGattServer.clearServices();
            mGattServer.close();
            mGattServer = null;
        }
    }

    private void restartServer() {
        stopAdvertising();
        stopServer();
        setupServer();
        startAdvertising();
    }

    private void updateCharacteristics(String prefStringId){
        String value = "";
        if(isstaticCharacteristic){
            value = String.valueOf(mBinding.staticValue.getSelectedItem().toString().charAt(0));
            if(mBinding.selectCharacteristicText.getText().toString().equalsIgnoreCase("Moving")){
                if(value.equalsIgnoreCase("1")){
                    mBinding.movingSwitch.setChecked(true);
                } else {
                    mBinding.movingSwitch.setChecked(false);
                }
            } else if(mBinding.selectCharacteristicText.getText().toString().equalsIgnoreCase("Eng/Ign")){
                if(value.equalsIgnoreCase("1")){
                    mBinding.ignitionSwitch.setChecked(true);
                } else {
                    mBinding.ignitionSwitch.setChecked(false);
                }

            }
        } else {
            value = mBinding.dynamicValue.getText().toString();
        }
        if(prefStringId != null && prefStringId.equalsIgnoreCase("5eed665c-0390-11e5-8418-1697f925ec7b")){
            int valueInt = Integer.parseInt(value);
            if(valueInt == 1) {
                long tripCurrentTime = serverfragment.getSystemTimeInSeconds();
                editor.putString("starttriptime", String.valueOf(tripCurrentTime));
                long tripOdoTrue = serverfragment.getOdoTrue();
                editor.putString("starttripodo", String.valueOf(tripOdoTrue));
                long startenginehours  = serverfragment.getECUEngineHours();
                editor.putString("starttripenginehours", String.valueOf(startenginehours));
            }
        }
        //editor.putString(prefStringId, "0x" + makeDoubleDigit(valueInHex(value)));
        try{
            if(prefStringId.equalsIgnoreCase("speed")){
                speed = Integer.parseInt(value);

            }

            if(prefStringId.equalsIgnoreCase("VIN")||prefStringId.equalsIgnoreCase("imei"))
            {
                if (prefStringId.equalsIgnoreCase("VIN"))
                {
                    try {
                        if (!Util.vinCheck(value)) {
                            Toast.makeText(getContext(), "Please enter a valid VIN", Toast.LENGTH_LONG).show();


                        } else {
                            vin = value;
                            editor.putString(prefStringId, value);
                            editor.commit();
                            Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception ex)
                    {
                        Toast.makeText(getContext(), "Inavalid VIN",Toast.LENGTH_LONG).show();
                    }

                }
                if (prefStringId.equalsIgnoreCase("imei"))
                {
                    if (value != null && value.length() > 0) {
                        imei = value;
                        editor.putString(prefStringId, value);
                        editor.commit();
                        Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.",Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Please enter a valid IMEI Number", Toast.LENGTH_LONG).show();
                    }

                }
            }else
            {
                editor.putString(prefStringId, value);
                editor.commit();
                Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.",Toast.LENGTH_SHORT).show();
            }

        }catch(Exception ex){
            Log.e("error", "error", ex);
        }

    }

    private void updateCharacteristics(String prefStringId, String value){
        try {
            if (prefStringId.equalsIgnoreCase("VIN") || prefStringId.equalsIgnoreCase("IMEI")) {
                if (prefStringId.equalsIgnoreCase("VIN")) ;
                {
                    if (!Util.vinCheck(value)) {
                        Toast.makeText(getContext(), "Please enter a valid VIN", Toast.LENGTH_LONG).show();
                        value = "";

                    } else {
                        vin = value;
                        editor.putString(prefStringId, value);
                        editor.commit();
                        Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.", Toast.LENGTH_SHORT).show();
                    }

                }
                if (prefStringId.equalsIgnoreCase("IMEI")) {
                    if (value != null && value.length() > 0) {
                        imei = value;
                        editor.putString(prefStringId, value);
                        editor.commit();
                        Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Please enter a valid IMEI Number", Toast.LENGTH_LONG).show();
                    }

                }
            } else {
                editor.putString(prefStringId, value);
                editor.commit();
                Toast.makeText(getContext(), getResources().getString(getCharacteristicStringId(prefStringId)) + " updated to " + value + ". Please restart server for changes to take effect.", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // Advertising
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void startAdvertising() {
        if (mBluetoothLeAdvertiser == null) {
            return;
        }

        logCharacteristicValues();

        AdvertiseSettings settings = new AdvertiseSettings.Builder().setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
                .build();

        UUID serviceUUID = java.util.UUID.fromString(getResources().getString(R.string.service_uuid));

        ParcelUuid parcelUuid = new ParcelUuid(serviceUUID);

        AdvertiseData data = new AdvertiseData.Builder().setIncludeDeviceName(true)
                .addServiceUuid(parcelUuid)
                .build();

        mBluetoothLeAdvertiser.startAdvertising(settings, data, mAdvertiseCallback);
    }

    private static long longFromMacAddress(String macAddress) {
        return Long.parseLong(macAddress.replaceAll(":", ""), 16);
    }

    private static String macAddressFromLong(long macAddressLong) {
        return String.format("%02x:%02x:%02x:%02x:%02x:%02x",
                (byte) (macAddressLong >> 40),
                (byte) (macAddressLong >> 32),
                (byte) (macAddressLong >> 24),
                (byte) (macAddressLong >> 16),
                (byte) (macAddressLong >> 8),
                (byte) (macAddressLong)).toUpperCase();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void stopAdvertising() {
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            log("Peripheral advertising started.");
        }

        @Override
        public void onStartFailure(int errorCode) {
            log("Peripheral advertising failed: " + errorCode);
        }
    };

    protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


    public boolean setCharacteristicNotification(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic characteristic,boolean enable) {
        bluetoothGatt.setCharacteristicNotification(characteristic, enable);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
        descriptor.setValue(enable ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : new byte[]{0x00, 0x00});
        return bluetoothGatt.writeDescriptor(descriptor); //descriptor write operation successfully started?

    }

    // Notifications
    private void notifyCharacteristic() {
        /*mHandler.post(() -> {
            UUID serviceUUID = java.util.UUID.fromString(getResources().getString(R.string.service_uuid));
            BluetoothGattService service = mGattServer.getService(serviceUUID);
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuid);
            log("Notifying characteristic " + characteristic.getUuid().toString()
                    + ", new value: " + StringUtils.byteArrayInHexFormat(value));

            characteristic.setValue(value);
            boolean confirm = BluetoothUtils.requiresConfirmation(characteristic);
            for(BluetoothDevice device : mDevices) {
                mGattServer.notifyCharacteristicChanged(device, characteristic, confirm);
            }
        });*/
        UUID uuid = UUID.fromString("e7737830-1018-11e6-a148-3e1d05defe78");
        UUID serviceUUID = java.util.UUID.fromString(getResources().getString(R.string.service_uuid));
        //BluetoothGattService service = mGattServer.getService(serviceUUID);
        //BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuid);

        try{
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try{

                        while(true){
                            if (mDevices.isEmpty()) {
                                Log.i(TAG, "No subscribers registered");
                                continue;
                            }

                            byte [] value = getCurrentGpsInfo();

                            for(BluetoothDevice device : mDevices) {
                                BluetoothGattCharacteristic gpsCharacteristic = mGattServer
                                        .getService(serviceUUID)
                                        .getCharacteristic(uuid);
                                gpsCharacteristic.setValue(value);

                                boolean confirm = BluetoothUtils.requiresConfirmation(gpsCharacteristic);
                                log("Notifying characteristic " + gpsCharacteristic.getUuid().toString()
                                        + ", new value: " + StringUtils.byteArrayInHexFormat(value) + " Confirm: " + confirm);
                                mGattServer.notifyCharacteristicChanged(device, gpsCharacteristic, confirm);
                            }
                            Thread.sleep(3000);
                        }
                    }catch(Exception ex){
                        Log.e("error", "error", ex);
                    }

                }
            }

            );
            t.start();

        }catch(Exception ex){
            Log.e("error", "error", ex);
        }
    }

    // Logging
    public void log(String msg) {
        Log.d(TAG, msg);
        mLogHandler.post(() -> {
            mBinding.viewServerLog.logTextView.append(msg + "\n");
            mBinding.viewServerLog.logScrollView.post(() -> mBinding.viewServerLog.logScrollView.fullScroll(View.FOCUS_DOWN));
        });
    }

    private void clearLogs() {
        mLogHandler.post(() -> mBinding.viewServerLog.logTextView.setText(""));
    }

    // Gatt Server Action Listener
    public void addDevice(BluetoothDevice device) {
        log("Device added: " + device.getAddress());
        mLogHandler.post(() -> mBinding.bleState.setChecked(true));
        mHandler.post(() -> mDevices.add(device));
    }

    public void removeDevice(BluetoothDevice device) {
        log("Device removed: " + device.getAddress());
        mLogHandler.post(() -> mBinding.bleState.setChecked(false));
        mHandler.post(() -> mDevices.remove(device));
    }

    public void sendResponse(BluetoothDevice device, int requestId, int status, int offset, byte[] value) {
        mHandler.post(() -> mGattServer.sendResponse(device, requestId, status, offset, value));
    }

    private void sendReverseMessage(byte[] message) {
        mHandler.post(() -> {
            // Reverse message to differentiate original message & response
            byte[] response = ByteUtils.reverse(message);
            log("Sending: " + StringUtils.byteArrayInHexFormat(response));
        });
    }

    // Gatt Callback
    private class GattServerCallback extends BluetoothGattServerCallback {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            super.onConnectionStateChange(device, status, newState);
            log("onConnectionStateChange " + device.getAddress()
                    + "\nstatus " + status
                    + "\nnewState " + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                blestatus=true;
                addDevice(device);
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                blestatus=false;
                removeDevice(device);
            }
        }

        // The Gatt will reject Characteristic Read requests that do not have the permission set,
        // so there is no need to check inside the callback
        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device,
                                                int requestId,
                                                int offset,
                                                BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);

            log("onCharacteristicReadRequest "
                    + characteristic.getUuid().toString());

            //if (BluetoothUtils.requiresResponse(characteristic)) {
            // Unknown read characteristic requiring response, send failure
            //  sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null);
            // }
            //String value = pref.getString(getPrefStringId(characteristic.getUuid().toString()), valueInHex("0"));
            //String value = "5TDKK3DC3DS292058";

            String uuid = characteristic.getUuid().toString();
            if(uuid != null ){
                if(uuid.equalsIgnoreCase("a9b9f487-5e60-43d5-a249-4d1d3f317d7e")){
                    //vin
                    byte[] bytes = new byte[0];
                    try {
                        String vin = pref.getString("vin", "");
                        if (VIN_EMPTY){
                            vin = "                 ";
                        }
                        StringBuilder str = new StringBuilder(vin);
                        char c = '\0';
                        str.append(c);

                        bytes = str.toString().getBytes("UTF-8");
                        //bytesnew[0] = new Integer(
                        //bytesnew= hexStringToByteArray(value);

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("5eed659a-0390-11e5-8418-1697f925ec7b")){
                    //moving
                    byte[] bytes = getCurrentMoving();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("5eed665c-0390-11e5-8418-1697f925ec7b")){
                    //Eng/Ign
                    byte[] bytes = getCurrentIgnition();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("48969a7e-39d5-11e8-b467-0ed5f89f718b")){
                    //System Unix Time
                    //byte [] bytes = ByteBuffer.allocate(8).putLong(getCurrentTime()).array();
                    byte [] bytes = getCurrentTime();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("5eed6d4e-0390-11e5-8418-1697f925ec7b")){
                    //odo
                    byte [] bytes = getCurrentOdoTrue();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("5eed6719-0390-11e5-8418-1697f925ec7b")){
                    //odo derived
                    byte [] bytes = getCurrentOdoTrue();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }
                else if(uuid.equalsIgnoreCase("5eed6ea2-0390-11e5-8418-1697f925ec7b")){
                    //speed
                    byte [] bytes = getCurrentSpeed();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("5eed6477-0390-11e5-8418-1697f925ec7b")){
                    //protocol
                    byte [] bytes = getCurrentProtocol();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("0d407e4c-39c9-11e8-b467-0ed5f89f718b")){
                    //ECU Eng Hours
                    //Seconds
                    byte [] bytes = getCurrentECUEngineHours();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("95dbace5-fcee-467f-bbe9-fe42e195bb04")){
                    //ECU Eng Hours
                    //Seconds
                    byte [] bytes = getCurrentECUEngineHours();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("87e22dd2-e27b-4094-968a-03e1537e7eb7")){
                    //ECU Eng Hours
                    //Seconds
                    byte [] bytes = getCurrentECUEngineHours();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }
                else if(uuid.equalsIgnoreCase("0f834728-39d5-11e8-b467-0ed5f89f718b")){
                    //Start Trip System Unix Time
                    byte [] bytes = getCurrentTripStartTime();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("5eed70e6-0390-11e5-8418-1697f925ec7b")){
                    //ECM
                    byte [] bytes = getCurrentECM();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("d4fb1556-39d2-11e8-b467-0ed5f89f718b")){
                    //Start trip engine hours
                    byte [] bytes = getCurrentECUEngineHoursStart();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("40418a44-9b8d-49f8-8ef2-afcea1643786")){
                    //Start trip engine hours
                    byte [] bytes = getCurrentECUEngineHoursStart();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("ba4ac777-52ec-4133-9f18-adad8b98f142")){
                    //Start trip odo
                    byte [] bytes = getCurrentTripOdoTrueStart();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("5ceb24b2-c273-40b4-b84b-f499e14c3f09")){
                    //Start trip odo
                    byte [] bytes = getCurrentTripOdoTrueStart();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("e7737830-1018-11e6-a148-3e1d05defe78")){
                    //GPS Info
                    byte [] bytes = getCurrentGpsInfo();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }else if(uuid.equalsIgnoreCase("0dd2af9d-7220-474f-8b34-4a94a6a97498")){
                    //External power
                    byte [] bytes = getCurrentExternalPower();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                } else if(uuid.equalsIgnoreCase("1c3917a6-7d33-4152-9a33-858b6f1fc99b")){
                    //Fuel
                    byte [] bytes = getCurrentFuel();
                    sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, bytes);
                }
            }

            // Not one of our characteristics or has NO_RESPONSE property set
        }

        // The Gatt will reject Characteristic Write requests that do not have the permission set,
        // so there is no need to check inside the callback
        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device,
                                                 int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {
            super.onCharacteristicWriteRequest(device,
                    requestId,
                    characteristic,
                    preparedWrite,
                    responseNeeded,
                    offset,
                    value);
            log("onCharacteristicWriteRequest" + characteristic.getUuid().toString()
                    + "\nReceived: " + StringUtils.byteArrayInHexFormat(value));

            sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null);
            sendReverseMessage(value);

            updateCharacteristics(characteristic.getUuid().toString(), StringUtils.byteArrayInHexFormat(value));
        }

        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
            log("onNotificationSent");
            byte [] bytes = getCurrentGpsInfo();
            sendResponse(device, 1, BluetoothGatt.GATT_SUCCESS, 0, bytes);
            try {
            } catch (Exception ex) {
                Log.w(TAG, "Unhandled exception: " + ex);
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset,
                                            BluetoothGattDescriptor descriptor) {
            if (CLIENT_CONFIG.equals(descriptor.getUuid())) {
                Log.d(TAG, "Config descriptor read");
                byte[] returnValue;
                if (mDevices.contains(device)) {
                    returnValue = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
                } else {
                    returnValue = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                }
                mGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_FAILURE,
                        0,
                        returnValue);
            } else {
                Log.w(TAG, "Unknown descriptor read request");
                mGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_FAILURE,
                        0,
                        null);
            }
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite, boolean responseNeeded,
                                             int offset, byte[] value) {
            if (CLIENT_CONFIG.equals(descriptor.getUuid())) {
                if (Arrays.equals(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE, value)) {
                    Log.d(TAG, "Subscribe device to notifications: " + device);
                    mDevices.add(device);
                } else if (Arrays.equals(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE, value)) {
                    Log.d(TAG, "Unsubscribe device from notifications: " + device);
                    mDevices.remove(device);
                }

                if (responseNeeded) {
                    mGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            null);
                }
            } else {
                Log.w(TAG, "Unknown descriptor write request");
                if (responseNeeded) {
                    mGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_FAILURE,
                            0,
                            null);
                }
            }
        }
    }

    public BluetoothGattCharacteristic createReadCharacteristic(String UUIDstr, String value){
        UUID readUUID = java.util.UUID.fromString(UUIDstr);
        BluetoothGattCharacteristic readCharacteristic = new BluetoothGattCharacteristic(readUUID,
                BluetoothGattCharacteristic.PROPERTY_READ,
                BluetoothGattCharacteristic.PERMISSION_READ);
        readCharacteristic.setValue(value);

        return readCharacteristic;
    }

    public BluetoothGattCharacteristic createReadCharacteristic(String UUIDstr){
        UUID readUUID = java.util.UUID.fromString(UUIDstr);
        BluetoothGattCharacteristic readCharacteristic = null;
        String prefStringId = getPrefStringId(UUIDstr);

        int properties = getCharacteristicProperties(prefStringId);
        readCharacteristic = new BluetoothGattCharacteristic(readUUID,
                properties,
                BluetoothGattCharacteristic.PERMISSION_READ);

        if(prefStringId.equalsIgnoreCase("vin")){
            char vinVal = pref.getString(prefStringId, "0").charAt(0);
            int vinAscii = toASCII(vinVal);
            readCharacteristic.setValue(valueInHex(vinAscii));
        } else {
            // readCharacteristic.setValue(valueInHex(pref.getString(prefStringId, "0")));
            readCharacteristic.setValue(pref.getString(prefStringId, "0"));
        }

        return readCharacteristic;
    }

    public void selectCharacteristicDialog(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.select_characteristic, null);
        dialogBuilder.setView(dialogView);
        selectCharacteristicAlert = dialogBuilder.create();
        Characteristic_recycler_View = (RecyclerView) dialogView.findViewById(R.id.recycler_view);
        Adapter = new CharacteristicsAdapter(text);
        Characteristic_recycler_View.setAdapter(Adapter);
        Characteristic_recycler_View.setHasFixedSize(true);
        Characteristic_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));
        TextView back = (TextView) dialogView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectCharacteristicAlert.dismiss();
            }
        });

        if(text.size() != 0) {
            text = new ArrayList<>();
            Adapter.notifyDataSetChanged();
        }

        try{
            for(String characteristic: characteristic_list){
                setNewLayout(characteristic);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        selectCharacteristicAlert.show();
    }

    public void setNewLayout(String txt){
        text.add(txt);

        Adapter.notifyDataSetChanged();
    }

    public class CharacteristicsAdapter extends RecyclerView.Adapter<CharacteristicsAdapter.MyViewHolder> {
        ArrayList<String> Text = new ArrayList<>();
        View layout_view;

        public CharacteristicsAdapter(ArrayList<String> text) {
            this.Text = text;
        }

        @Override
        public CharacteristicsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            layout_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new CharacteristicsAdapter.MyViewHolder(layout_view);
        }

        @Override
        public void onBindViewHolder(CharacteristicsAdapter.MyViewHolder holder, final int position)
        {
            try {
                holder.text.setText(Text.get(position));
                holder.text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        editor.putString("characteristic", characteristic_list[position]);
                        editor.putString("position", String.valueOf(position));
                        editor.commit();
                        selectCharacteristicAlert.dismiss();
                        setSelectedCharacteristicAndValues();
                    }
                });

                layout_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {

                    }
                });
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return text.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;

            public MyViewHolder(View view) {
                super(view);
                text = (TextView) view.findViewById(R.id.text);
            }
        }
    }

    private void logCharacteristicValues(){
        log("-------------------------");
        String[] characteristics = getResources().getStringArray(R.array.characteristic_list);
        String[] characteristicsids = getResources().getStringArray(R.array.characteristic_ids);
        for(int i = 0; i < characteristics.length; i++) {
            log(characteristics[i] + ": " + pref.getString(characteristicsids[i], ""));
        }
    }

    public byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static long getSystemTimeInSeconds(){
        return (System.currentTimeMillis())/(1000);
        //return 1000;
    }

    public byte[] getCurrentTime(){
        long nowLong = serverfragment.getSystemTimeInSeconds();
        String nowHex = Long.toHexString(nowLong);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentTripStartTime(){
        long tripStartTime = 0;
        //long tripStartTime = Long.parseLong(pref.getString("starttriptime", "0"));

        if(pref != null){

            try{
                String tripStartTimeStr = pref.getString(PropertyEnum.Start_Trip_System_Unix_Time.getValue(), "0");
                tripStartTime = Long.parseLong(tripStartTimeStr);
            }catch(Exception ex){
                Log.e("error", "error", ex);
            }
        }

        String nowHex = Long.toHexString(tripStartTime);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public static long getOdoTrue(){
        //return (System.currentTimeMillis() - 1538352000)/(1000 * 60 * 60);
        odoG = odoG + 1000;
        return odoG;
    }

    public static void updateOdo(int eventcode){
        if(odoPrevTimeStamp == null && eventcode == 6011){
            odoPrevTimeStamp = new Date();
        }
        if(eventcode == 4001 || eventcode == 6012){
            if(odoPrevTimeStamp == null){
                odoPrevTimeStamp = new Date();
            }
            if(odoPrevTimeStamp != null){
                long diffTimeInSec = Util.timeDiffInSeconds(new Date(), odoPrevTimeStamp);
                Log.i("iii", "Diff in seconds: " + diffTimeInSec);
                if(speed > 0){
                    //long speedInMeterPerHour = (long)(speed/1.609) * 1000;
                    double meterPerSecond = speed / 2.237;
                    long meterPerSecondActual = Math.round(meterPerSecond * diffTimeInSec);
                    Log.i("iii","second:" + diffTimeInSec + " Distance in meters travelled: " + meterPerSecondActual);
                    odo = odo + meterPerSecondActual;
                    //updateValue(PropertyEnum.True_Odo.getValue(), String.valueOf(odo));
                    System.out.println("Odo new: " + odo);
                    odoPrevTimeStamp = new Date();
                }
            }
        }
    }

    public byte[] getCurrentOdoTrue(){
        odoG = odoG + 1000;

        if(ignition == 1){
            if(odoPrevTimeStamp == null){
                odoPrevTimeStamp = new Date();
            }
            if(odoPrevTimeStamp != null){
                long diffTimeInSec = Util.timeDiffInSeconds(new Date(), odoPrevTimeStamp);
                Log.i("iii", "Diff in seconds: " + diffTimeInSec);
                if(speed > 0){
                    long speedInMeterPerHour = (long)(speed/1.609) * 1000;
                    double distanceTraveledInMeters = speedInMeterPerHour * (diffTimeInSec / 3600.0);
                    Log.i("iii","distanceTraveledInMeters:" + distanceTraveledInMeters);
                    odo = odo + (long)distanceTraveledInMeters;
                    updateValue(PropertyEnum.True_Odo.getValue(), String.valueOf(odo));
                    System.out.println("Odo new: " + odo);
                    odoPrevTimeStamp = new Date();
                }
            }
        }

        long nowLong = odo;
        String nowHex = Long.toHexString(nowLong);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));
        //stack.push(nowHex.substring(8,10));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }
        str.append("00");

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentTripOdoTrue(){

        long nowLong = 0;

        try{
            //String nowStr = pref.getString(PropertyEnum.Start_Trip_Derived_Odom.getValue(), "0");
            odoG = odoG + 1000;
            String nowStr = String.valueOf(odoG);
            if(nowStr != null && !nowStr.isEmpty()){
                nowLong = Long.parseLong(nowStr);
            }
        }catch(Exception ex){
            Log.e("error", "error", ex);
        }
        String nowHex = Long.toHexString(nowLong);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));
        //stack.push(nowHex.substring(8,10));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }
        str.append("00");

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentTripOdoTrueStart(){

        long nowLong = 0;

        try{
            //String nowStr = pref.getString(PropertyEnum.Start_Trip_Derived_Odom.getValue(), "0");
            /*String nowStr = String.valueOf(odoGStart);

            if(nowStr != null && !nowStr.isEmpty()){
                nowLong = Long.parseLong(nowStr);
            }*/
        }catch(Exception ex){
            Log.e("error", "error", ex);
        }
        String nowHex = Long.toHexString(odoStart);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));
        //stack.push(nowHex.substring(8,10));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }
        str.append("00");

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }


    public static long getECUEngineHours(){
        return serverfragment.getSystemTimeInSeconds() + 20;
    }

    public static void updateEngineHours(int eventcode){
        if(eventcode == 6011){
            engineHoursPrevTimeStamp = new Date();
        }
        if(eventcode == 4001 || eventcode == 6012){
            if(engineHoursPrevTimeStamp != null){
                Date now = new Date();
                long durationInSeconds = Util.timeDiffInSeconds(now,engineHoursPrevTimeStamp);
                engineHours = engineHours + durationInSeconds;
                //updateValue(PropertyEnum.ECU_Eng_Hours_Seconds.getValue(), String.valueOf(engineHours));
                engineHoursPrevTimeStamp = now;
            } else {
                engineHoursPrevTimeStamp = new Date();
            }
        }
    }

    public byte[] getCurrentECUEngineHours(){
        secondsG = secondsG + 5000;
        long nowLong =secondsG;

        if(engineHoursPrevTimeStamp == null){
            engineHoursPrevTimeStamp = new Date();
        }

        if(engineHoursPrevTimeStamp == null){
            engineHoursPrevTimeStamp = new Date();
        }


        if(ignition == 1){
            if(engineHoursPrevTimeStamp != null){
                Date now = new Date();
                long durationInSeconds = Util.timeDiffInSeconds(now,engineHoursPrevTimeStamp);
                engineHours = engineHours + durationInSeconds;
                updateValue(PropertyEnum.ECU_Eng_Hours_Seconds.getValue(), String.valueOf(engineHours));
                engineHoursPrevTimeStamp = now;
            }
        }

        String nowHex = Long.toHexString(engineHours);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));
        //stack.push(nowHex.substring(8,10));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentECUEngineHoursStart(){
        //long nowLong = secondsGStart;

        /*try{
            //String nowStr = pref.getString(PropertyEnum.Start_Trip_ECU_Eng_Hours_Seconds.getValue(), "0");
            String nowStr = String.valueOf(nowLong);
            if(nowStr != null && !nowStr.isEmpty()){
                nowLong = Long.parseLong(nowStr);
            }
        }catch(Exception ex){
            Log.e("error", "error", ex);
        }*/


        String nowHex = Long.toHexString(engineHoursStart);
        nowHex = padLeft(nowHex, 8);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));
        stack.push(nowHex.substring(4,6));
        stack.push(nowHex.substring(6,8));
        //stack.push(nowHex.substring(8,10));

        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentSpeed(){
        int speedmiles = Integer.parseInt(pref.getString( "speed", "0"));
        int nowSpeed = Math.round( speedmiles * 16);


        String nowHex = Integer.toHexString(nowSpeed);
        nowHex = padLeft(nowHex, 4);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));


        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }
        str.append("00");

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentFuel(){
        int fuel = Integer.parseInt(pref.getString( "fuel_level", "750").replace(".", ""));
        //int nowSpeed = Math.round( speedmiles * 16);

        Log.i("iii", "Fuel: " + fuel);

        String nowHex = Integer.toHexString(fuel);

        Log.i("iii", "Fuel: " + nowHex);

        nowHex = padLeft(nowHex, 4);

        Log.i("iii", "Fuel: " + nowHex);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));
        stack.push(nowHex.substring(2,4));


        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }
        //str.append("00");

        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public byte[] getCurrentMoving(){
        int moving = Integer.parseInt(pref.getString( "moving", "0")) ;


        String nowHex = Integer.toHexString(moving);
        nowHex = padLeft(nowHex, 2);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));



        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        return bytenew;
    }

    public byte[] getCurrentIgnition(){
        int ignition =0;
        try{
            String ignitionStr = pref.getString( "eng_ign", "0");
            if(ignitionStr != null && !ignitionStr.isEmpty()){
                ignition =  Integer.parseInt(ignitionStr);
            }

        }catch(Exception ex){
            Log.e("error", "error", ex);
        }


        String nowHex = Integer.toHexString(ignition);
        nowHex = padLeft(nowHex, 2);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));



        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        return bytenew;
    }


    public byte[] getCurrentProtocol(){
        int protocol = Integer.parseInt(pref.getString( "protocol", "0")) ;


        String nowHex = Integer.toHexString(protocol);
        nowHex = padLeft(nowHex, 2);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));



        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        return bytenew;
    }

    public byte[] getCurrentECM(){
        int ecm = Integer.parseInt(pref.getString( "ecm", "0")) ;


        String nowHex = Integer.toHexString(ecm);
        nowHex = padLeft(nowHex, 2);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));



        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        return bytenew;
    }

    public byte[] getCurrentExternalPower(){
        int externalpower = Integer.parseInt(pref.getString( "external_power", "0")) ;

        Log.i("iii", "External power: " + externalpower);


        String nowHex = Integer.toHexString(externalpower);
        nowHex = padLeft(nowHex, 2);

        Stack<String> stack = new Stack<>();
        stack.push(nowHex.substring(0,2));



        StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }


        String finalStr = str.toString().replace(":", "");
        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        return bytenew;
    }

    public static byte[] hexStringToByteArrayNew(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }

    public byte[] getCurrentGpsInfo(){
        long nowLong = Long.parseLong(pref.getString("starttripenginehours", "0"));
        int lockInt = 0;

        try{
            lockInt = Integer.parseInt(pref.getString("gps_info", "4"));
        }catch (Exception ex) {
            Log.e("error", "error", ex);
        }

        //Queue<String> stack = new Linke<>();
        //stack.push();

        long epochSeconds = System.currentTimeMillis()/1000;
        StringBuilder str = new StringBuilder();

        String unix = Long.toHexString(epochSeconds);
        unix = padLeft(unix, 8);
        String[] valuesUnix = splitToNChar(unix, 2);
        str.append(valuesUnix[3]).append(valuesUnix[2]).append(valuesUnix[1]).append(valuesUnix[0]);

        /*String latitudeStr = "-121.944753";
        String longitudeStr = "37.742462";
        int lat = Integer.parseInt(latitudeStr.replace(".", ""));
        int lng = Integer.parseInt(longitudeStr.replace(".", ""));*/

        //str.append(padLeft(Integer.toHexString(lat).toUpperCase(), 8));
        //str.append(padLeft(Integer.toHexString(lng).toUpperCase(), 8));

        //old
        //str.append("79E73F02");
        //str.append("3C45BBF8");

        GPSInfo gpsInfo = gpsQueue.poll();

        str.append(gpsInfo.getLatitude());
        str.append(gpsInfo.getLongitude());
        gpsQueue.add(gpsInfo);


        //String longitude = Long.toHexString(0);
        //longitude = padLeft(longitude, 8);
        //stack.push(longitude);
        //str.append(longitude);

        String speed = Integer.toHexString(0);
        speed = padLeft(speed, 2);
        //stack.push(speed);
        str.append(speed);

        String heading = Integer.toHexString(0);
        heading = padLeft(heading, 4);
        str.append(heading);
        //stack.push(heading);

        String hdop = Integer.toHexString(0);
        hdop = padLeft(hdop, 4);
        str.append(hdop);
        //stack.push(hdop);

        String sats = Integer.toHexString(0);
        sats = padLeft(sats, 2);
        str.append(sats);
        //stack.push(sats);

        String lock = Integer.toHexString(lockInt);
        lock = padLeft(lock, 2);
        str.append(lock);
        //stack.push(lock);


       /* StringBuilder str = new StringBuilder();
        while(!stack.isEmpty()){
            if (str.length() > 0){
                str.append(":");
            }
            str.append(stack.pop());
        }*/


        String finalStr = str.toString().replace(":", "");

        Log.i("iii", "gpsinfo: " + finalStr);

        byte[] bytenew = hexStringToByteArrayNew(finalStr);
        //long reverseDecimal = Long.parseLong(finalStr, 16);
        //return reverseDecimal;
        return bytenew;
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s).replace(' ', '0');
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s).replace(' ', '0');
    }

    private static String[] splitToNChar(String text, int size)
    {
        List<String> parts = new ArrayList<>();
        int length = text.length();
        for (int i = 0; i < length; i += size)
        {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }

    public void updateValue(String property, String value){
        pref = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(property, value);
        editor.commit();
    }

    public String getValue(String property, String defaultValue){
        pref = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String str = pref.getString(property, defaultValue);
        return str;
    }
    public String getServerAddress()
    {
        String ip="";
        try
        {
           pref=getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
           ip=pref.getString("server","");
           if(ip.equalsIgnoreCase("prod"))
           {
               ip="18.213.67.94";
           }else if(ip.equalsIgnoreCase("test"))
           {
               ip="54.164.84.102";
           }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  ip;
    }
}
