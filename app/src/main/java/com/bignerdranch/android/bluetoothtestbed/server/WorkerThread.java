package com.bignerdranch.android.bluetoothtestbed.server;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import java.util.Date;

public class WorkerThread implements Runnable {

    BluetoothGattCharacteristic characteristic;

    public WorkerThread() {

    }

    public WorkerThread(BluetoothGattCharacteristic characteristic) {
        this.characteristic = characteristic;

    }

    @Override
    public void run() {
        while(true){
            try{

                Thread.sleep(2000);
            }catch(Exception ex){
                Log.e("error", "error", ex);
            }
        }
    }
}

