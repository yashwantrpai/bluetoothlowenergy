package com.bignerdranch.android.bluetoothtestbed.server;

public class XT6300Cellular {


    public static String getTemplate(){
        return "$deviceid$packetid$reasoncode$datetimestamp$latitude$longitude$altitude$speed$heading$rpm$numstats$hdop$derivedodo$voltage$receiversigstr$packetserialnumber$vin$systemstates$obdfuellevelpct$obdtrueodometer$obdtotengsecs$obdtotdrivingsec$startderivedodo$starttrueenginehours$starttrueodo$obdlifetimeodom$obdprotocols$obdcommsstate$imei$obdenginesecs$obdengcoolanttemp$fuelpercentage$startderivedenginehours";
    }

    public static String getDeviceId(){
        return "0AEB7D63";
    }

    public static String getPacketId(){
        return "00";
    }

    public static String getReasonCode(int code){
        int ignitionCode = 0 ;

        if (code == 6011){
            ignitionCode = 11;
        }
        if (code == 6012){
            ignitionCode = 12;
        }
        if (code == 4001){
            ignitionCode = 129;
        }
        if (code == 4002){
            ignitionCode = 130;
        }

        return UtilCellular.padLeft(Integer.toHexString(ignitionCode), 2).toUpperCase();
    }

    public static String getDateTimeStamp(){
        long unixTime = System.currentTimeMillis() / 1000L;
        String nowHex = Long.toHexString(unixTime);
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getLatitude(double latitude){
        long lat = Math.round(latitude * 1000000.0);
        String hex = Long.toHexString(lat);
        return UtilCellular.padLeft(hex, 8).toUpperCase();
        //return  latitude;
    }

    public static String getLongitude(double longitude){
        long lng = Math.round(longitude * 1000000.0);
        String hex = Long.toHexString(lng);
        hex =  UtilCellular.padLeft(hex, 8).toUpperCase();
        return hex.substring(hex.length() - 8, hex.length());
        //return  longitude;
    }

    public static  String getAltitude(){
        return "044D";
    }

    public static String getSpeed(double speedmiles){
        long nowSpeed = Math.round( speedmiles * 16);
        return UtilCellular.padLeft(Long.toHexString(nowSpeed), 4).toUpperCase();
    }

    public static String getHeading(){
        return "08A7";
    }

    public static String getRPM(){
        return "0000";
    }

    public static String getNumStats(){
        return "12";
    }

    public static String getHdop(){
        return "06";
    }

    public static String getVoltage(){
        return "8B";
    }

    public static String getReceiverSigStr(){
        return "000B";
    }

    public static String getPacketSerialNumber(){
        return "0104";
    }

    public static String getVin(String vin){

        if(serverfragment.VIN_EMPTY){
            return "2020202020202020202020202020202020";
        }
        return UtilCellular.padLeft(UtilCellular.getAciiToHex(vin), 34).toUpperCase();

    }

    public static String getSystemStates(boolean bluetoothState){
        String value = "0";
        if(bluetoothState){
            value = "1";
        }
        return UtilCellular.padLeft(UtilCellular.convertBinaryToHexadecimal("100011100$bluetoothstate1101111".replace("$bluetoothstate", value)), 8).toUpperCase();
    }

    public static String getObdFuelLevelPct(long fuel){
        return UtilCellular.padLeft(Long.toHexString(fuel), 4).toUpperCase();
    }

    public static String getOdoTrue(int eventcode){
        serverfragment.updateOdo(eventcode);
        //String nowHex = Long.toHexString(Math.round(serverfragment.odo * 1609.344));
        String nowHex = Long.toHexString(Math.round(serverfragment.odo));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }

        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getOdoTrueStart(long odoTrueStart, int eventcode){
        //String nowHex = Long.toHexString(Math.round(odoTrueStart * 1609.344));
        String nowHex = Long.toHexString(Math.round(odoTrueStart));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getOdoDerived(int eventcode){
        serverfragment.updateOdo(eventcode);
        //String nowHex = Long.toHexString(Math.round(serverfragment.odo * 1609.344));
        String nowHex = Long.toHexString(Math.round(serverfragment.odo ));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getOdoDerivedStart(long odoDerivedStart, int eventcode){
        //String nowHex = Long.toHexString(Math.round(odoDerivedStart * 1609.344));
        String nowHex = Long.toHexString(Math.round(odoDerivedStart));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getEngineHoursSecondsTrue(int eventcode){
        serverfragment.updateEngineHours(eventcode);
        //String nowHex = Long.toHexString(Math.round(serverfragment.engineHours * 3600));
        String nowHex = Long.toHexString(Math.round(serverfragment.engineHours));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getEngineHoursSecondsTrueStart(double engineHours, int eventcode){
        //String nowHex = Long.toHexString(Math.round(engineHours * 3600));
        String nowHex = Long.toHexString(Math.round(engineHours));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getEngineHoursSecondsDerived(int eventcode){
        serverfragment.updateEngineHours(eventcode);
        //String nowHex = Long.toHexString(Math.round(serverfragment.engineHours * 3600));
        String nowHex = Long.toHexString(Math.round(serverfragment.engineHours));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getEngineHoursSecondsDerivedStart(double engineHours, int eventcode){
        //String nowHex = Long.toHexString(Math.round(engineHours * 3600));
        String nowHex = Long.toHexString(Math.round(engineHours));
        if(eventcode == 6012 && serverfragment.PROTOCOL_ZERO_6012){
            nowHex = Long.toHexString(0);
        }
        nowHex = UtilCellular.padLeft(nowHex, 8);
        return nowHex.toUpperCase();
    }

    public static String getProtocols(String protocol){
        if(protocol.equalsIgnoreCase("jbus")){
            return "00000040";
        }
        if(protocol.equalsIgnoreCase("obdii")){
            return "00000080";
        }
        if(protocol.equalsIgnoreCase("none")){
            return "00000001";
        }
        return "00000001";
    }

    public static String getECM(int ecm){
        return UtilCellular.padLeft(Integer.toHexString(ecm), 2).toUpperCase();
    }

    public static String getImei(String imei){
        return UtilCellular.padRight(UtilCellular.getAciiToHex(imei), 48).toUpperCase();
    }

    public static String getCoolantTemp(){
        return "0000";
    }


    public static String constructPacket(XT6300Packet packet){
        return getTemplate()
                .replace("$deviceid", packet.getDeviceId())
                .replace("$packetid", packet.getPacketId())
                .replace("$reasoncode", packet.getReasonCode())
                .replace("$datetimestamp",packet.getDatetimeStamp())
                .replace("$latitude", packet.getLatitude())
                .replace("$longitude", packet.getLongitude())
                .replace("$altitude", packet.getAltitude())
                .replace("$speed", packet.getSpeed())
                .replace("$heading", packet.getHeading())
                .replace("$rpm", packet.getRpm())
                .replace("$numstats", packet.getNumstats())
                .replace("$hdop", packet.getHdop())
                .replace("$derivedodo", packet.getDerivedOdo())
                .replace("$voltage", packet.getVoltage())
                .replace("$receiversigstr", packet.getReceiverSigStr())
                .replace("$packetserialnumber", packet.getPacketSerialNumber())
                .replace("$vin", packet.getVin())
                .replace("$systemstates", packet.getSystemStates())
                .replace("$obdfuellevelpct", packet.getOdbFuellevelPct())
                .replace("$obdtrueodometer", packet.getObdTrueOdometer())
                .replace("$obdtotengsecs", packet.getObdTotEngSecs())
                .replace("$obdtotdrivingsec", packet.getObdTotDrivingSec())
                .replace("$startderivedodo", packet.getStartDerivedOdo())
                .replace("$starttrueenginehours", packet.getStartTrueEngineHours())
                .replace("$starttrueodo", packet.getStartTrueOdo())
                .replace("$obdlifetimeodom",packet.getObdLifeTimeodom())
                .replace("$obdprotocols", packet.getObdProtocols())
                .replace("$obdcommsstate", packet.getObdCommsState())
                .replace("$imei", packet.getImei())
                .replace("$obdenginesecs", packet.getObdEngineSecs())
                .replace("$obdengcoolanttemp", packet.getObdEngCoolantTemp())
                .replace("$fuelpercentage", packet.getOdbFuellevelPct())
                .replace("$startderivedenginehours", packet.getStartDerivedEngineHours());

    }

    public static void main(String[] args){
        XT6300Cellular obj = new XT6300Cellular();
        XT6300Packet packet = new XT6300Packet();
        packet.setDeviceId(obj.getDeviceId());
        packet.setPacketId(obj.getPacketId());
        packet.setReasonCode(obj.getReasonCode(6011));
        packet.setDatetimeStamp(obj.getDateTimeStamp());
        packet.setLatitude(obj.getLatitude(37.734430));
        packet.setLongitude(obj.getLongitude(-121.941373));
        packet.setAltitude(obj.getAltitude());
        packet.setSpeed(obj.getSpeed(0));
        packet.setHeading(obj.getHeading());
        packet.setRpm(obj.getRPM());
        packet.setNumstats(obj.getNumStats());
        packet.setHdop(obj.getHdop());
        packet.setDerivedOdo(obj.getOdoDerived(23));
        packet.setVoltage(obj.getVoltage());
        packet.setReceiverSigStr(obj.getReceiverSigStr());
        packet.setPacketSerialNumber(obj.getPacketSerialNumber());
        packet.setVin(obj.getVin("5TDKK3DC3DS292058"));
        packet.setSystemStates(obj.getSystemStates(true));
        packet.setOdbFuellevelPct(obj.getObdFuelLevelPct(362));
        packet.setObdTrueOdometer(obj.getOdoTrue(23));
        packet.setObdTotEngSecs(obj.getEngineHoursSecondsTrue(2));
        packet.setObdTotDrivingSec(obj.getEngineHoursSecondsTrue(2));
        packet.setStartDerivedOdo(obj.getOdoDerivedStart(20, 6011));
        packet.setStartTrueEngineHours(obj.getEngineHoursSecondsTrueStart(20, 6011));
        packet.setStartTrueOdo(obj.getOdoTrueStart(23, 6011));
        packet.setObdLifeTimeodom(obj.getOdoDerived(23));
        packet.setObdProtocols(obj.getProtocols("jbus"));
        packet.setObdCommsState(obj.getECM(3));
        packet.setImei(obj.getImei("357298071278385"));
        packet.setObdEngineSecs(obj.getEngineHoursSecondsDerived(23));
        packet.setObdEngCoolantTemp(obj.getCoolantTemp());
        packet.setStartDerivedEngineHours(obj.getEngineHoursSecondsDerivedStart(23, 6011));

        System.out.println(obj.constructPacket(packet));


        System.out.println("vin:" + obj.getVin("4T1BF1FK2CU516164"));
        System.out.println("Lat:" + obj.getLatitude(37.734430) );
        System.out.println("Lat:" + obj.getLongitude(-121.941373) );
        System.out.println("ECM:" + obj.getECM(3));
        System.out.println("imei:" + obj.getImei("357298071278385"));
        System.out.println("Reason code: " + obj.getReasonCode(4002));
        System.out.println("Speed: " + obj.getSpeed(3.7290242386575514));
        System.out.println("Fuel: " + obj.getObdFuelLevelPct(362));
    }
}


